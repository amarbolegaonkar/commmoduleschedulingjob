﻿using Microsoft.WindowsAzure.Storage;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.Azure;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using System.Net.Mail;
using log4net;
using System.Collections;
using System.Text.RegularExpressions;
using Newtonsoft.Json;

namespace CommModuleScheduleEvents
{
    class Program
    {
        //static BoyceDBProdBackup_27NovEntities entity = new BoyceDBProdBackup_27NovEntities();
        private static ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        static string dbCon;
        static SqlCommand cmd;
        static DataTable dt = new DataTable();
        static SqlDataAdapter adp;
        static DateTime currentDate = DateTime.UtcNow.Date;
        static string cmdStr = string.Empty;
        static string path = System.Environment.CurrentDirectory;// System.IO.Path.GetDirectoryName(Environment.GetCommandLineArgs()[0]);
        //static string path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

        static string pathImage = Convert.ToString(ConfigurationManager.AppSettings["HostPath"]);

        static string checkedChkBox = pathImage + "Content\\Images\\checkbox-1.jpg";
        static string unchkedChkBox = pathImage + "Content\\Images\\Grey-checkbox.jpg";
        static string crossChkBox = pathImage + "Content\\Images\\Grey-checkbox-cross.jpg";

        static string uncheckChkImg = "<img src=" + unchkedChkBox + "  height='15px' width='15px'>";
        static string checkedChkImg = "<img src=" + checkedChkBox + "  height='20px' width='20px'>";
        static string crossChkImg = "<img src=" + crossChkBox + "  height='20px' width='20px'>";

        static void Main(string[] args)
        {
            try
            {
                string sendgridApiKey = ConfigurationManager.AppSettings["ApiKeySendgrid"];
                dbCon = ConfigurationManager.ConnectionStrings["DbCon"].ConnectionString;

                GetScheduledEvents(dbCon);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                logger.Error(ex.StackTrace);
                logger.Error(ex.InnerException);
            }
        }


        /// <summary>
        /// To Get Scheduled Element For Current Date
        /// </summary>
        /// <param name="dbCon"></param>
        static void GetScheduledEvents(string dbCon)
        {
            List<PortalCommModuleScheduledEvents> activeScheduleEventList = new List<PortalCommModuleScheduledEvents>();
            SqlConnection con = new SqlConnection(dbCon);
            // currentDate = Convert.ToDateTime("2019-12-09 00:00:00.000");

            cmdStr = "Select * from PortalCommModule_ScheduledEvents where IsActive=1 and NextDeliveryDate='" + currentDate + "'";

            dt = ExecuteSqlCommand(cmdStr, con);

            activeScheduleEventList = ConvertDataTableToList<PortalCommModuleScheduledEvents>(dt);

            cmdStr = "select CompanyEmailUrl from PortalDefaults_EmailSettings";
            dt = ExecuteSqlCommand(cmdStr, con);
            string emailFromUrl = dt.Rows[0][0].ToString();

            cmdStr = "select CompanyName from PortalDefaults_LoginScreen";
            dt = ExecuteSqlCommand(cmdStr, con);
            string companyName = dt.Rows[0][0].ToString();

            foreach (PortalCommModuleScheduledEvents scheduleEvent in activeScheduleEventList)
            {
                //sendBulkMail(path, scheduleEvent.TemplateBody, scheduleEvent.TemplateId, scheduleEvent.ContactIds,scheduleEvent.ScheduleToList,scheduleEvent.ClientId, scheduleEvent.NoticeType, scheduleEvent.Attachments);

                sendBulkMail(path, scheduleEvent, emailFromUrl, companyName);
            }
        }



        /// <summary>
        /// Send Bulk Email
        /// </summary>
        /// <param name="sitePath"></param>
        /// <param name="scheduleEvent"></param>
        /// <param name="emailFromUrl"></param>
        /// <param name="companyName"></param>
        public static void sendBulkMail(string sitePath, PortalCommModuleScheduledEvents scheduleEvent, string emailFromUrl, string companyName)
        {

            SqlConnection con = new SqlConnection(dbCon);
            List<CommLogPopUpModel> commLogDetailsList = new List<CommLogPopUpModel>();
            CommLogPopUpModel commLogDetails = new CommLogPopUpModel();
            List<GeneralIdNameModel> selectedContactAdvisorList = new List<GeneralIdNameModel>();
            GeneralIdNameModel selectedContactAdvisorobj = new GeneralIdNameModel();
            PortalCommModuleTemplates templateDetails = new PortalCommModuleTemplates();
            string attachments = scheduleEvent.Attachments;
            string templateBody = scheduleEvent.TemplateBody;
            int templateId = scheduleEvent.TemplateId;
            string contactIdsStr = scheduleEvent.ContactIds;
            string emailIdStr = scheduleEvent.ScheduleToList;
            string clientIdStr = scheduleEvent.ClientId;
            cmdStr = string.Empty;
            string toMailStr = string.Empty;
            List<ClientIdEmailModel> clientEmailList = new List<ClientIdEmailModel>();
            string emailBodyContent = string.Empty;
            string bodyContent = templateBody;
            string apikey = Convert.ToString(ConfigurationManager.AppSettings["ApiKeySendgrid"]);
            // string apikey = "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";
            var client = new SendGridClient(apikey);

            cmdStr = "select * from PortalCommModule_Templates where Id=" + templateId;
            dt = ExecuteSqlCommand(cmdStr, con);
            List<PortalCommModuleTemplates> templateList = ConvertDataTableToList<PortalCommModuleTemplates>(dt);
            templateDetails = templateList[0];
            string templateSubject = templateDetails.TemplateSubject;

            var from = new EmailAddress(emailFromUrl, companyName);
            var tos = new List<EmailAddress>();
            var substitutions = new List<Dictionary<string, string>>();
            var subjects = new List<string>();

            string[] contactIdsArr = contactIdsStr.Split(';');
            string[] emailIdArr = emailIdStr.Split(';');
            string[] clientIdArr = clientIdStr.Split(';');

            cmdStr = "select * from PortalCommModule_PredefinedKey";
            dt = ExecuteSqlCommand(cmdStr, con);
            List<CommModule_Predefined> predefinedKey = ConvertDataTableToList<CommModule_Predefined>(dt);
            var predefinedKeyValue = predefinedKey.Where(x => x.PredefinedKey == "[FollowupsCheckList]").Select(x => x.PredefinedValue).FirstOrDefault();
            bool isblank = bodyContent.Contains(predefinedKeyValue);
            string isfollowups = string.Empty;
            int count = 0;

            foreach (string contact in contactIdsArr)
            {
                CommModule_AllPredefinedKey obj = new CommModule_AllPredefinedKey();
                selectedContactAdvisorobj = new GeneralIdNameModel();
                ClientIdEmailModel clientIdEmailItem = new ClientIdEmailModel();
                Dictionary<string, string> dynamicField = new Dictionary<string, string>();
                bool isFollowupdata = true;

                string emailStr = emailIdArr[count];
                string[] contactListArr = contact.Split('~');
                string[] emailAddr = emailStr.Split('~');
                clientIdEmailItem.ClientId = Convert.ToInt32(clientIdArr[count]);

                int contactId = Convert.ToInt32(contactListArr[2]);
                string contactType=contactListArr[0];
                //for every plan///////////
                List<Client_info> clientdata = new List<Client_info>();
                cmdStr = " select py.PYE_Year,p.Plans_Index_ID,p.Plan_Number,p.Plan_Name,p.PlanYE,cl.Client_Name,cl.Client_ID from Plans p join Plans_YearToYear py on p.Plans_Index_ID=py.Plans_Index_ID  join Client_Master cl on cl.Client_ID= p.Client_ID where p.Client_ID=" + clientIdEmailItem.ClientId + " and py.ActivePlanYear=" + 1 + " and py.IsHoldFollowUp=" + 0 + " ";
                dt = ExecuteSqlCommand(cmdStr, con);
                clientdata = ConvertDataTableToList<Client_info>(dt);
                if (clientdata.Count > 0)
                {
                    obj.CilentName = clientdata[0].Client_Name;
                }
               emailBodyContent = bodyContent;
               obj = getFollowUpInformation(templateDetails.FollowupType, clientIdEmailItem.ClientId, con, obj, bodyContent, contactId, contactType, isblank);

               if(templateDetails.NoticeType == 5)
               {
                   if(obj.followUpData == null)
                   {
                       isFollowupdata = false;
                   }
               }
               if (isFollowupdata == false)
               {
                   continue;
               }

                // string temp = Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase);
                string temp = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                temp = "http://boyce-webportal-staging.azurewebsites.net//" + obj.signature;
                // var path = DownloadAttachmentFromBlob(temp, obj.signature, true);

                //  string sign = SignaturePicture(filterLetterInfo[0].CSR, pathImage + "Content//ContactImages//");

                string imgf = "<img src=" + temp + "  height='120px' width='120px'>";
                obj.signature = imgf;
                string Followup_Signature = string.Empty;
                Followup_Signature = "<p style='margin:0;padding:0;'>" + obj.signature;
                Followup_Signature = Followup_Signature + "<p style='margin:0;padding:0;'>" + obj.PAame;
                Followup_Signature = Followup_Signature + "<p style='margin:0;padding:0;'>" + obj.Title;
                Followup_Signature = Followup_Signature + "<p style='margin:0;padding:0;'>" + obj.userinitial;
                //Followup_Signature = Followup_Signature + "<p style='margin:0;padding:0;margin-left:40px;'>" + obj.ccInformation;

                obj.Footer = Followup_Signature;

                int emailContactId = Convert.ToInt32(emailAddr[0]);

                selectedContactAdvisorobj.ClientID = clientIdEmailItem.ClientId;
                selectedContactAdvisorobj.EmailId = emailAddr[1];
                selectedContactAdvisorobj.ContactMethod = 1;
                selectedContactAdvisorobj.ID = contactId;
                selectedContactAdvisorobj.IsChecked = false;
                selectedContactAdvisorobj.Name = contactListArr[0];
                selectedContactAdvisorobj.PlanTypeId = -1;
                selectedContactAdvisorobj.UserInitial = null;

                if (contactId == emailContactId)
                {

                    // tos.Add(new EmailAddress("vandana.pund@thinkinghut.in"));
                     tos.Add(new EmailAddress(emailAddr[1]));
                     foreach (CommModule_Predefined key in predefinedKey)
                     {
                         var findstr = bodyContent.Contains(key.PredefinedValue);
                         if (findstr == true)
                         {
                             switch (key.PredefinedKey)
                             {
                                 case "[FollowupsCheckList]":
                                     dynamicField.Add(key.PredefinedValue, obj.Followups);
                                     emailBodyContent = emailBodyContent.Replace(key.PredefinedValue, obj.Followups);
                                     break;

                                 case "[Followup1_KEY]":
                                     obj.FUP1 = "Followup Letter1";
                                     templateDetails.FollowupType = 1;
                                     dynamicField.Add(key.PredefinedValue, obj.FUP1);
                                     emailBodyContent = emailBodyContent.Replace(key.PredefinedValue, obj.Followups);
                                     break;

                                 case "[Followup2_KEY]":
                                     obj.FUP1 = "Followup Letter2";
                                     templateDetails.FollowupType = 2;
                                     dynamicField.Add(key.PredefinedValue, obj.FUP1);
                                     emailBodyContent = emailBodyContent.Replace(key.PredefinedValue, obj.FUP1);
                                     break;

                                 case "[Followup3_KEY]":
                                     templateDetails.FollowupType = 3;
                                     obj.FUP1 = "Followup Letter3";
                                     dynamicField.Add(key.PredefinedValue, obj.FUP1);
                                     emailBodyContent = emailBodyContent.Replace(key.PredefinedValue, obj.FUP1);
                                     break;

                                 case "[Followup4_KEY]":
                                     templateDetails.FollowupType = 4;
                                     obj.FUP1 = "Followup Letter4";
                                     dynamicField.Add(key.PredefinedValue, obj.FUP1);
                                     emailBodyContent = emailBodyContent.Replace(key.PredefinedValue, obj.FUP1);
                                     break;

                                 case "[Followup_KEY_InitiateNewYear]":
                                     obj.FUP1 = "Initial Followup Letter";
                                     templateDetails.FollowupType = 5;
                                     dynamicField.Add(key.PredefinedValue, obj.Followups);
                                     emailBodyContent = emailBodyContent.Replace(key.PredefinedValue, obj.Followups);
                                     break;

                                 case "[CurrentDate]":
                                     dynamicField.Add(key.PredefinedValue, obj.Date);
                                     emailBodyContent = emailBodyContent.Replace(key.PredefinedValue, obj.Date);
                                     break;

                                 case "[ContactFirstName]":
                                     dynamicField.Add(key.PredefinedValue, obj.FirstName);
                                     emailBodyContent = emailBodyContent.Replace(key.PredefinedValue, obj.FirstName);
                                     break;

                                 case "[ContactMI]":
                                     dynamicField.Add(key.PredefinedValue, obj.MI);
                                     emailBodyContent = emailBodyContent.Replace(key.PredefinedValue, obj.MI);
                                     break;

                                 case "[ContactLastName]":
                                     dynamicField.Add(key.PredefinedValue, obj.LastName);
                                     emailBodyContent = emailBodyContent.Replace(key.PredefinedValue, obj.LastName);
                                     break;

                                 case "[ClientName]":
                                     dynamicField.Add(key.PredefinedValue, obj.CilentName);
                                     emailBodyContent = emailBodyContent.Replace(key.PredefinedValue, obj.CilentName);
                                     break;

                                 case "[MailAddress1]":
                                     dynamicField.Add(key.PredefinedValue, obj.Address1);
                                     emailBodyContent = emailBodyContent.Replace(key.PredefinedValue, obj.Address1);
                                     break;

                                 case "[MailAddress2]":
                                     dynamicField.Add(key.PredefinedValue, obj.Address2);
                                     emailBodyContent = emailBodyContent.Replace(key.PredefinedValue, obj.Address2);
                                     break;

                                 case "[MailCity]":
                                     dynamicField.Add(key.PredefinedValue, obj.City);
                                     emailBodyContent = emailBodyContent.Replace(key.PredefinedValue, obj.City);
                                     break;

                                 case "[MailState_abv]":
                                     dynamicField.Add(key.PredefinedValue, obj.State_abv);
                                     emailBodyContent = emailBodyContent.Replace(key.PredefinedValue, obj.State_abv);
                                     break;

                                 case "[MailZipCode]":
                                     dynamicField.Add(key.PredefinedValue, obj.zipcode);
                                     emailBodyContent = emailBodyContent.Replace(key.PredefinedValue, obj.zipcode);
                                     break;

                                 case "[PhysicalAddress1]":
                                     dynamicField.Add(key.PredefinedValue, obj.PhysicalAddress1);
                                     emailBodyContent = emailBodyContent.Replace(key.PredefinedValue, obj.PhysicalAddress1);
                                     break;

                                 case "[PhysicalAddress2]":
                                     dynamicField.Add(key.PredefinedValue, obj.PhysicalAddress2);
                                     emailBodyContent = emailBodyContent.Replace(key.PredefinedValue, obj.PhysicalAddress2);
                                     break;

                                 case "[PhysicalCity]":
                                     dynamicField.Add(key.PredefinedValue, obj.PhysicalCity);
                                     emailBodyContent = emailBodyContent.Replace(key.PredefinedValue, obj.PhysicalCity);
                                     break;

                                 case "[PhysicalState_abv]":
                                     dynamicField.Add(key.PredefinedValue, obj.PhysicalState_abv);
                                     emailBodyContent = emailBodyContent.Replace(key.PredefinedValue, obj.PhysicalState_abv);
                                     break;

                                 case "[PhysicalZipCode]":
                                     dynamicField.Add(key.PredefinedValue, obj.PhysicalZipCode);
                                     emailBodyContent = emailBodyContent.Replace(key.PredefinedValue, obj.PhysicalZipCode);
                                     break;

                                 case "[PlanName]":
                                     dynamicField.Add(key.PredefinedValue, obj.planName);
                                     emailBodyContent = emailBodyContent.Replace(key.PredefinedValue, obj.planName);
                                     break;

                                 case "[PlanNumber]":
                                     dynamicField.Add(key.PredefinedValue, obj.PlanNumber);
                                     emailBodyContent = emailBodyContent.Replace(key.PredefinedValue, obj.PlanNumber);
                                     break;

                                 case "[ContactGreetingText]":

                                     dynamicField.Add(key.PredefinedValue, obj.Greetingtext);
                                     emailBodyContent = emailBodyContent.Replace(key.PredefinedValue, obj.Greetingtext);
                                     break;

                                 case "[UnExtendedFileDeadlineDate]":
                                     dynamicField.Add(key.PredefinedValue, obj.UnExtDate);
                                     emailBodyContent = emailBodyContent.Replace(key.PredefinedValue, obj.UnExtDate);
                                     break;

                                 case "[PlanYearEndDate]":
                                     dynamicField.Add(key.PredefinedValue, obj.planyearEndDate);
                                     emailBodyContent = emailBodyContent.Replace(key.PredefinedValue, obj.planyearEndDate);
                                     break;

                                 case "[1stRushChgStartDate]":

                                     dynamicField.Add(key.PredefinedValue, obj.StartDate);
                                     emailBodyContent = emailBodyContent.Replace(key.PredefinedValue, obj.StartDate);
                                     break;

                                 case "[1stRushChgEndDate]":
                                     dynamicField.Add(key.PredefinedValue, obj.EndDate);
                                     emailBodyContent = emailBodyContent.Replace(key.PredefinedValue, obj.EndDate);
                                     break;

                                 case "[ExtendedFileDeadlineDate]":
                                     dynamicField.Add(key.PredefinedValue, obj.extendeddate);
                                     emailBodyContent = emailBodyContent.Replace(key.PredefinedValue, obj.extendeddate);
                                     break;

                                 case "[2ndRushChgStartDate]":
                                     dynamicField.Add(key.PredefinedValue, obj.secondndRushChgStartDate);
                                     emailBodyContent = emailBodyContent.Replace(key.PredefinedValue, obj.secondndRushChgStartDate);
                                     break;

                                 case "[2ndRushChgEndDate]":
                                     dynamicField.Add(key.PredefinedValue, obj.secondndRushChgEndDate);
                                     emailBodyContent = emailBodyContent.Replace(key.PredefinedValue, obj.secondndRushChgEndDate);
                                     break;

                                 case "[ContributionFundingDeadlineDate]":
                                     dynamicField.Add(key.PredefinedValue, obj.ContriButionDateFundingDeadline);
                                     emailBodyContent = emailBodyContent.Replace(key.PredefinedValue, obj.ContriButionDateFundingDeadline);
                                     break;

                                 case "[MinFundingDeadlineDate]":
                                     dynamicField.Add(key.PredefinedValue, obj.MinmumFundingDeadLine);
                                     emailBodyContent = emailBodyContent.Replace(key.PredefinedValue, obj.MinmumFundingDeadLine);
                                     break;

                                 case "[TPAUserName]":
                                     dynamicField.Add(key.PredefinedValue, obj.PAame);
                                     emailBodyContent = emailBodyContent.Replace(key.PredefinedValue, obj.PAame);
                                     break;

                                 case "[TPAUserTitle]":
                                     dynamicField.Add(key.PredefinedValue, obj.Title);
                                     emailBodyContent = emailBodyContent.Replace(key.PredefinedValue, obj.Title);
                                     break;

                                 case "[TPAUserInitials]":
                                     dynamicField.Add(key.PredefinedValue, obj.CsrInitial);
                                     emailBodyContent = emailBodyContent.Replace(key.PredefinedValue, obj.CsrInitial);
                                     break;

                                 case "[TPAUserPhone]":
                                     dynamicField.Add(key.PredefinedValue, obj.CSRPhone);
                                     emailBodyContent = emailBodyContent.Replace(key.PredefinedValue, obj.CSRPhone);
                                     break;

                                 case "[TPAUserEmail]":
                                     dynamicField.Add(key.PredefinedValue, obj.CSREmail);
                                     emailBodyContent = emailBodyContent.Replace(key.PredefinedValue, obj.CSREmail);
                                     break;

                                 case "[TPAUserImage]":
                                     dynamicField.Add(key.PredefinedValue, obj.signature);
                                     emailBodyContent = emailBodyContent.Replace(key.PredefinedValue, obj.signature);
                                     break;

                                 case "[ccInformation]":
                                     dynamicField.Add(key.PredefinedValue, obj.ccInformation);
                                     emailBodyContent = emailBodyContent.Replace(key.PredefinedValue, obj.ccInformation);
                                     break;

                                 case "[ContactAddressBlock]":
                                     dynamicField.Add(key.PredefinedValue, obj.Header);
                                     emailBodyContent = emailBodyContent.Replace(key.PredefinedValue, obj.Header);
                                     break;

                                 case "[Followup_Signature]":

                                     dynamicField.Add(key.PredefinedValue, obj.Footer);
                                     emailBodyContent = emailBodyContent.Replace(key.PredefinedValue, obj.Footer);
                                     break;

                                 case "[5500ExtensionRequiredDate]":
                                     dynamicField.Add(key.PredefinedValue, obj.Ext5500_RequiredDate);
                                     emailBodyContent = emailBodyContent.Replace(key.PredefinedValue, obj.Ext5500_RequiredDate);
                                     break;
                             }
                         }
                    }
                    substitutions.Add(dynamicField);
                    subjects.Add(templateSubject);
                    clientIdEmailItem.Email = emailAddr[1];
                    clientIdEmailItem.Type = contactListArr[0];
                    clientIdEmailItem.ContactAdvisorId = Convert.ToInt32(contactListArr[2]);
                    clientIdEmailItem.ContactAdvisorName = contactListArr[1];
                    clientIdEmailItem.emailContent = emailBodyContent;
                    clientEmailList.Add(clientIdEmailItem);

                    commLogDetails.Subject = templateSubject;
                    commLogDetails.Description = emailBodyContent;
                    commLogDetails.AttachmentId = scheduleEvent.AttachmentId;
                    commLogDetails.ClientId = clientIdEmailItem.ClientId;
                    commLogDetailsList.Add(commLogDetails);
                }
                //tos.Add(new EmailAddress(emailIdArr[count]));              
                count++;
                selectedContactAdvisorList.Add(selectedContactAdvisorobj);
                //}
            }


            var plainTextContent = "Hello-Name";
            var htmlContent = bodyContent;
            var msg = MailHelper.CreateMultipleEmailsToMultipleRecipients(from,
                                                                          tos,
                                                                          subjects,
                                                                          plainTextContent,
                                                                          htmlContent,
                                                                          substitutions
                                                                          );

            msg.AddHeader("Priority", "Urgent");
            msg.AddHeader("Importance", "high");


            bool isSend = true;
            //string filename = attachments;
            string[] attachmentArr = attachments.Split(';');
            if (attachmentArr[0] != "")
            {
                foreach (string fileName in attachmentArr)
                {
                    string filePath = "CommModule/Attachments/" + fileName;
                    var path = DownloadAttachmentFromBlob(sitePath, filePath, isSend);
                    if (path != "")
                    {
                        var bytes = File.ReadAllBytes(path);
                        var file = Convert.ToBase64String(bytes);
                        msg.AddAttachment(filePath, file);
                    }
                }
            }
            int commLogId = -1;

            var resp = client.SendEmailAsync(msg).Wait(60000);

            if (resp)
            {
                List<string> clientIdlst = clientIdArr.Distinct().ToList();

                commLogDetails.Subject = templateSubject;
                //commLogDetails.Description = bodyContent;
                commLogDetails.AttachmentId = scheduleEvent.AttachmentId;
                scheduleEvent.OccurancesCompleted = scheduleEvent.OccurancesCompleted + 1;
                int? Bywho = scheduleEvent.ScheduleCreatedBy;
                AddEmailRecord(clientEmailList, scheduleEvent, con, emailFromUrl, companyName, templateSubject);
                if (scheduleEvent.AddToCommLog == true)
                {
                    string result = SaveCommLogDetails(commLogDetails, selectedContactAdvisorList, Bywho, ref commLogId, commLogDetailsList);
                }
                if (scheduleEvent.NoticeType == 5 && templateDetails.FollowupType != 0)
                {
                    SetPlanYearEnd(clientIdlst, con, templateDetails.FollowupType, scheduleEvent.ScheduleCreatedBy);
                }
            }
            SetNextDeliveryDate(scheduleEvent, con);
        }


        /// <summary>
        /// Set Next Delivery Date
        /// </summary>
        static void SetNextDeliveryDate(PortalCommModuleScheduledEvents scheduleEvent, SqlConnection con)
        {
            int nextDay = 0;
            int daysToAdd = 0;
            string deActiveScheduleStr = "Update PortalCommModule_ScheduledEvents set IsActive=0, DeliveredDate='" + currentDate + "', OccurancesCompleted=" + scheduleEvent.OccurancesCompleted + " where ScheduleId=" + scheduleEvent.ScheduleId;
            string nextDelivertDtStr = string.Empty;
            DateTime nextDeliveryDate = new DateTime();
            cmdStr = string.Empty;

            if (scheduleEvent.ScheduleType == 5)
            {
                cmdStr = deActiveScheduleStr;
                ExecuteSqlCommand(cmdStr, con);
            }
            else if (scheduleEvent.ScheduleType == 1)
            {
                nextDelivertDtStr = "Update PortalCommModule_ScheduledEvents set NextDeliveryDate='" + currentDate.AddDays(1) + "', DeliveredDate='" + currentDate + "', OccurancesCompleted=" + scheduleEvent.OccurancesCompleted + " where ScheduleId=" + scheduleEvent.ScheduleId;

                UpdateNextDeliveryDate(scheduleEvent, nextDelivertDtStr, deActiveScheduleStr, nextDeliveryDate, con);
            }
            else if (scheduleEvent.ScheduleType == 2)
            {
                string weekDaysStr = scheduleEvent.ScheduleWeekDays;
                nextDeliveryDate = new DateTime();
                List<int> weekDayList=new List<int> ();
                if (weekDaysStr != "")
                {
                   weekDayList = weekDaysStr.Split(';').Select(int.Parse).ToList();
                }
                int currentWeekDay = GetCurrentDayIndex(currentDate.DayOfWeek.ToString());
                List<int> currentWeekList = weekDayList.Where(x => x > currentWeekDay).ToList();
                List<int> nextWeekList = weekDayList.Where(x => x <= currentWeekDay).ToList();
                int weekDaysCount = 7;
                if (scheduleEvent.WeekOccurances > 0)
                {
                    weekDaysCount = weekDaysCount * scheduleEvent.WeekOccurances.GetValueOrDefault();
                }
                if (currentWeekList.Count > 0)
                {
                    nextDay = currentWeekList.Min();
                    daysToAdd = nextDay - currentWeekDay;
                    nextDeliveryDate = currentDate.AddDays(daysToAdd);
                }
                else if (nextWeekList.Count > 0)
                {
                    int dayDiff = weekDaysCount - currentWeekDay;
                    nextDay = nextWeekList.Min();
                    daysToAdd = nextDay + dayDiff;
                    nextDeliveryDate = currentDate.AddDays(daysToAdd);
                }

                nextDelivertDtStr = "Update PortalCommModule_ScheduledEvents set NextDeliveryDate='" + nextDeliveryDate + "', DeliveredDate='" + currentDate + "', OccurancesCompleted=" + scheduleEvent.OccurancesCompleted + " where ScheduleId=" + scheduleEvent.ScheduleId;

                UpdateNextDeliveryDate(scheduleEvent, nextDelivertDtStr, deActiveScheduleStr, nextDeliveryDate, con);
            }
            else if (scheduleEvent.ScheduleType == 3)
            {
                nextDeliveryDate = new DateTime();

                nextDeliveryDate = scheduleEvent.NextDeliveryDate.GetValueOrDefault().AddMonths(1);

                nextDelivertDtStr = "Update PortalCommModule_ScheduledEvents set NextDeliveryDate='" + nextDeliveryDate + "', DeliveredDate='" + currentDate + "', OccurancesCompleted=" + scheduleEvent.OccurancesCompleted + " where ScheduleId=" + scheduleEvent.ScheduleId;

                UpdateNextDeliveryDate(scheduleEvent, nextDelivertDtStr, deActiveScheduleStr, nextDeliveryDate, con);
            }
            else if (scheduleEvent.ScheduleType == 4)
            {
                nextDeliveryDate = new DateTime();

                nextDeliveryDate = scheduleEvent.NextDeliveryDate.GetValueOrDefault().AddYears(1);

                nextDelivertDtStr = "Update PortalCommModule_ScheduledEvents set NextDeliveryDate='" + nextDeliveryDate + "', DeliveredDate='" + currentDate + "', OccurancesCompleted=" + scheduleEvent.OccurancesCompleted + " where ScheduleId=" + scheduleEvent.ScheduleId;

                UpdateNextDeliveryDate(scheduleEvent, nextDelivertDtStr, deActiveScheduleStr, nextDeliveryDate, con);
            }

        }

        /// <summary>
        /// Update Next Delivery Date
        /// </summary>
        static void UpdateNextDeliveryDate(PortalCommModuleScheduledEvents scheduleEvent, string nextDelivertDateStr, string deactiveScheduleStr, DateTime nextDeliveryDate, SqlConnection con)
        {
            string cmdStrToExecute = string.Empty;

            if (scheduleEvent.ScheduleEndDate != null && scheduleEvent.ScheduleEndDate >= nextDeliveryDate)
            {
                cmdStrToExecute = nextDelivertDateStr;
            }
            else if (scheduleEvent.EndByOccurances > 0 && scheduleEvent.EndByOccurances != scheduleEvent.OccurancesCompleted)
            {
                cmdStrToExecute = nextDelivertDateStr;
            }
            else
            {
                cmdStrToExecute = deactiveScheduleStr;
            }

            if (cmdStrToExecute != string.Empty)
                ExecuteSqlCommandUpdate(cmdStrToExecute, con);
        }

        /// <summary>
        /// Get Current DayIndex
        /// </summary>
        static int GetCurrentDayIndex(string day)
        {
            int dayIndex = 0;
            switch (day)
            {
                case "Monday":
                    dayIndex = 1;
                    break;
                case "Tuesday":
                    dayIndex = 2;
                    break;
                case "Wednesday":
                    dayIndex = 3;
                    break;
                case "Thursday":
                    dayIndex = 4;
                    break;
                case "Friday":
                    dayIndex = 5;
                    break;
                case "Saturday":
                    dayIndex = 6;
                    break;
                case "Sunday":
                    dayIndex = 7;
                    break;
            }
            return dayIndex;
        }

        /// <summary>
        /// Execute SqlCommand for Update
        /// </summary>
        static void ExecuteSqlCommandUpdate(string cmdString, SqlConnection con)
        {
            try
            {
                cmd = new SqlCommand(cmdString, con);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
            catch
            {
                con.Close();
            }
            finally
            {
                con.Close();
            }
        }

        /// <summary>
        /// Execute Scaler SqlCommand Update
        /// </summary>
        static string ExecuteScalerSqlCommandUpdate(string cmdString, SqlConnection con)
        {
            string result = null;
            try
            {
                string query2 = "Select @@Identity";
                // int ID;
                cmd = new SqlCommand(cmdString, con);
                con.Open();

                cmd.ExecuteNonQuery();
                cmd.CommandText = query2;
                var ID = cmd.ExecuteScalar();

                // var firstColumn = cmd.ExecuteScalar();

                if (ID != null)
                {
                    result = ID.ToString();
                }
                con.Close();
                return result;
            }
            catch
            {
                con.Close();

            }
            finally
            {
                con.Close();

            }
            return result;
        }

        /// <summary>
        /// Execute SqlCommand 
        /// </summary>
        public static DataTable ExecuteSqlCommand(string cmdString, SqlConnection con)
        {
            dt = new DataTable();
            cmd = new SqlCommand(cmdString, con);
            adp = new SqlDataAdapter(cmd);
            adp.Fill(dt);
            return dt;
        }

        /// <summary>
        /// Download Attachment From Blob
        /// </summary>
        public static string DownloadAttachmentFromBlob(string sitePath, string filePath, bool isSend)
        {
            string localPath = string.Empty;
            string fileHtmlText = string.Empty;
            PortalDefaultsBlobDetails blobDetails = new PortalDefaultsBlobDetails();
            cmdStr = string.Empty;
            SqlConnection con = new SqlConnection(dbCon);
            List<PortalDefaultsBlobDetails> blobDetailsList = new List<PortalDefaultsBlobDetails>();
            cmdStr = "Select * from PortalDefaults_BlobDetails";
            dt = ExecuteSqlCommand(cmdStr, con);
            blobDetailsList = ConvertDataTableToList<PortalDefaultsBlobDetails>(dt);
            blobDetails = blobDetailsList[0];

            //blobDetails = entity.PortalDefaults_BlobDetails.FirstOrDefault();
            string subContainerName = blobDetails.BlobContainerName;// Convert.ToString(ConfigurationManager.AppSettings["BlobContainerStore"]);
            string connectionKey = blobDetails.BlobConnectionKey;

            string blobName = "";
            string[] stringSeparators = new string[] { subContainerName };

            string downloadPath = sitePath.Trim('\\') + @"\" + "Downloads" + "\\" + subContainerName;

            if (Directory.Exists(downloadPath))
            {
                Directory.Delete(downloadPath, true);
            }

            blobName = filePath.Replace(@"\", @"/");
            blobName = blobName.Substring(0, 1) == "/" ? blobName.Remove(0, 1) : blobName;

            //string connectionKey = WebConfigurationManager.AppSettings["BlobConnectionKey"];

            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting(connectionKey));
            CloudBlobClient storageClient = storageAccount.CreateCloudBlobClient();
            CloudBlobContainer storageContainer = storageClient.GetContainerReference((subContainerName));

            CloudBlockBlob blobSource = storageContainer.GetBlockBlobReference(blobName);

            if (blobSource.Exists())
            {
                //blob storage uses forward slashes, windows uses backward slashes; do a replace
                //  so localPath will be right
                localPath = Path.Combine(@downloadPath, blobSource.Name.Replace(@"/", @"\").Replace(":", ""));
                //if the directory path matching the "folders" in the blob name don't exist, create them
                string dirPath = Path.GetDirectoryName(localPath);
                if (!Directory.Exists(localPath))
                {
                    Directory.CreateDirectory(dirPath);
                }
                blobSource.DownloadToFile(localPath, FileMode.Create);
            }
            string path = "";
            if (Directory.Exists(downloadPath))
            {
                path = localPath;
                fileHtmlText = File.ReadAllText(path);
            }
            if (isSend == true)
            {
                return path;
            }
            else
            {
                return fileHtmlText;
            }
        }

        /// <summary>
        /// Generic Method To "Convert DataTable into List"
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static List<T> ConvertDataTableToList<T>(DataTable dt)
        {
            List<T> data = new List<T>();
            foreach (DataRow dr in dt.Rows)
            {
                T item = GetItem<T>(dr);
                data.Add(item);
            }
            return data;
        }

        /// <summary>
        /// Internal function for 'ConvertDataTableToList' method
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dr"></param>
        /// <returns></returns>
        private static T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();

            foreach (DataColumn col in dr.Table.Columns)
            {
                foreach (PropertyInfo pro in temp.GetProperties())
                {
                    if (pro.Name == col.ColumnName)
                    {
                        if (dr[col.ColumnName] != DBNull.Value)
                            pro.SetValue(obj, dr[col.ColumnName], null);
                    }
                    else
                    {
                        continue;
                    }
                }
            }
            return obj;
        }

        /// <summary>
        ///Add Email Record
        /// </summary>
        static void AddEmailRecord(List<ClientIdEmailModel> clientEmailList, PortalCommModuleScheduledEvents scheduleEvent, SqlConnection con, string emailFromUrl, string companyName, string templateSubject)
        {
            //List<int> clientIdList = new List<int>();
            //clientIdList = clientEmailList.Select(x => x.ClientId).Distinct().ToList();
            foreach (ClientIdEmailModel clientObj in clientEmailList)
            {
                string insertRecordStr = string.Empty;
                string emailContent = string.Empty;
                emailContent = scheduleEvent.TemplateBody.Replace("[UserName]", clientObj.ContactAdvisorName);
                if (clientObj.Type == "Contact")
                {
                    //insertRecordStr = "Insert into EmailRecord Values('" + emailFromUrl + "','" + clientObj.Email + "','','" + templateSubject + "','" + emailContent + "'," + clientObj.ClientId + "," + clientObj.ContactAdvisorId + ",0,'" + currentDate + "'," + scheduleEvent.ScheduleCreatedBy + "," + scheduleEvent.AttachmentId + ",0,-1,'" + currentDate + "',0)";
                    insertRecordStr = "Insert into EmailRecord Values('" + emailFromUrl + "','" + clientObj.Email + "','','" + templateSubject + "','" + clientObj.emailContent + "'," + clientObj.ClientId + "," + clientObj.ContactAdvisorId + ",0,'" + currentDate + "'," + scheduleEvent.ScheduleCreatedBy + "," + scheduleEvent.AttachmentId + ",0,-1,'" + currentDate + "',0)";
                }
                else
                {
                    insertRecordStr = "Insert into EmailRecord Values('" + emailFromUrl + "','" + clientObj.Email + "','','" + templateSubject + "','" + clientObj.emailContent + "'," + clientObj.ClientId + ",0," + clientObj.ContactAdvisorId + ",'" + currentDate + "'," + scheduleEvent.ScheduleCreatedBy + "," + scheduleEvent.AttachmentId + ",0,-1,'" + currentDate + "',0)";
                }
                ExecuteSqlCommandUpdate(insertRecordStr, con);
            }
        }


         static string SaveCommLogDetails(CommLogPopUpModel commLogDetails, List<GeneralIdNameModel> selectedContactAdvisorList, int? Bywho, ref int commLogId, List<CommLogPopUpModel> commLogDetailsList)
        {
            string result = "Failure";
            var clientId = selectedContactAdvisorList.Select(x => x.ClientID).Distinct().ToList();
            foreach (CommLogPopUpModel item in commLogDetailsList)
            {
                commLogDetails.Description = item.Description;
                List<GeneralIdNameModel> ContactAdvisorList = selectedContactAdvisorList.Where(x => x.ClientID == item.ClientId).ToList();
                SqlConnection con = new SqlConnection(dbCon);
                string insertRecordStr = string.Empty;

                Comm_Log commLog = new Comm_Log();

                commLog.OriginDate = Convert.ToDateTime(commLogDetails.CreateDate);
                commLog.CreatedByWho = commLogDetails.LastTouchedBy;
                commLog.Attachment_ID = commLogDetails.AttachmentId;
                commLog = AssignCommLogValues(commLog, commLogDetails, false, item.ClientId, Bywho);

                insertRecordStr = "Insert into Comm_Log Values('" + commLog.Client_ID + "','" + commLog.Comm_Log_Type + "','" + commLog.Comm_Log_Origination + "','" + commLog.Comm_Log_Category + "','" + commLog.Sub_Category + "','" + commLog.Comm_Log_Status + "','" + commLog.Plan_ID + "','" + commLog.Project_ID + "','" + commLog.AdHocToDo + "','" + commLog.Title + "','" + commLog.CommContact1 + "','" + commLog.CommContact2 + "','" + commLog.CommContact3 + "','" + commLog.Attachment_ID + "','" + commLog.Comm_Log_Text + "','" + commLog.Duplicate + "','" + commLog.Internal_Use_Only + "','" + commLog.OriginDate + "','" + commLog.LastTouched + "','" + commLog.ByWho + "','" + commLog.Actual_Time + "','" + commLog.CreatedByWho + "','" + commLog.IsDeletedFromOutlook + "','" + commLog.IsPortalDeleted + "','" + commLog.ByWhoClient + "','" + commLog.LastTouchedClient + "','" + commLog.IsLastUpdatedByAdvisor + "')";
                string value = ExecuteScalerSqlCommandUpdate(insertRecordStr, con);
                commLog.Comm_Log_ID = Convert.ToInt32(value);
                commLogId = commLog.Comm_Log_ID;
                SetCommLogContactAdvisor(commLogId, ContactAdvisorList, false, con);
            }
            result = "Success";
            return result;
        }


        private static Comm_Log AssignCommLogValues(Comm_Log commLog, CommLogPopUpModel commLogDetails, bool isClientChange, int ClientId, int? Bywho)
        {
            commLog.Client_ID = ClientId;

            commLog.Comm_Log_Type = 1;
            commLog.Comm_Log_Origination = 2;
            if (isClientChange)
            {
                commLog.Plan_ID = -1;
            }
            else
            {
                commLog.Plan_ID = -1;
            }
            commLog.Comm_Log_Category = 16;
            commLog.Sub_Category = DateTime.UtcNow.Year.ToString();
            commLog.Actual_Time = Convert.ToDecimal(0.1);
            commLog.Duplicate = false;
            commLog.Internal_Use_Only = false;
            commLog.AdHocToDo = -1;
            commLog.Title = commLogDetails.Subject;
            commLog.Comm_Log_Text = commLogDetails.Description;
            commLog.Attachment_ID = commLogDetails.AttachmentId;
            commLog.LastTouched = DateTime.UtcNow;
            commLog.OriginDate = DateTime.UtcNow;
            commLog.ByWho =Convert.ToInt32(Bywho);
            commLog.ByWhoClient = -1;
            commLog.LastTouchedClient = DateTime.UtcNow;
            return commLog;
        }

        private static void SetCommLogContactAdvisor(int commLogId, List<GeneralIdNameModel> selectedContactAdvisorList, bool isClientChange, SqlConnection con)
        {
            Portal_CommLogContactAdvisor contactAdvisorRecord = new Portal_CommLogContactAdvisor();

            if (selectedContactAdvisorList.Count > 0)
            {
                foreach (GeneralIdNameModel record in selectedContactAdvisorList)
                {
                    string insertRecordStr = string.Empty;
                    // string emailContent = string.Empty;
                    contactAdvisorRecord = AddCommLogContactAdvisor(record, commLogId);
                    insertRecordStr = "Insert into Portal_CommLogContactAdvisor Values('" + contactAdvisorRecord.CommLogId + "','" + contactAdvisorRecord.ContactAdvisorId + "','" + contactAdvisorRecord.IsContactAdvisor + "','" + contactAdvisorRecord.IsSelected + "','" + contactAdvisorRecord.IsPortalDeleted + "')";
                    ExecuteSqlCommandUpdate(insertRecordStr, con);
                }

            }
        }

        private static Portal_CommLogContactAdvisor AddCommLogContactAdvisor(GeneralIdNameModel selectedRecord, int commLogId)
        {
            Portal_CommLogContactAdvisor contactAdvisorRecord = new Portal_CommLogContactAdvisor();

            contactAdvisorRecord.ContactAdvisorId = selectedRecord.ID;
            //contactAdvisorRecord.CommLogId = commLogId;
            contactAdvisorRecord.CommLogId = commLogId;
            contactAdvisorRecord.IsSelected = true;
            if (selectedRecord.Name.Contains("Contact"))
            {
                contactAdvisorRecord.IsContactAdvisor = "Contact";
            }
            else if (selectedRecord.Name.Contains("Advisor"))
            {
                contactAdvisorRecord.IsContactAdvisor = "Advisor";
            }
            contactAdvisorRecord.IsPortalDeleted = false;
            return contactAdvisorRecord;
        }

        /// <summary>
        /// Generate Followup letter1
        /// </summary>
        /// <param name="letterInfo"></param>
        /// <param name="cCInfo"></param>

        /// <param name="obj"></param>
        /// <returns></returns>
        public static string GenrateFollowupLetter1(List<List<LetterInfoModel>> letterInfo, List<LetterCCInfoModel> cCInfo, List<List<FollowUpLetterModel>> followUpInfo, int pYEYear,
         string userInitials, string hostPath, string htmlDoc, string dateOfRecord, int UID, string FileName, bool isblank, CommModule_AllPredefinedKey obj)
        {
            string htmlDoctemp = string.Empty;
            try
            {
                var date = (DateTime.UtcNow.Date).ToString("dd-MM-yyyy");
                var dtfi = new System.Globalization.DateTimeFormatInfo { ShortDatePattern = "MM/dd/yyyy", DateSeparator = "/" };
                //DateTime dtOfRecord = Convert.ToDateTime(dateOfRecord, dtfi);
                DateTime dtOfRecord = DateTime.UtcNow.Date;
                var filterLetterInfo = new List<LetterInfoModel>();
                List<DateTime> dt = new List<DateTime>();
                for (int i = 0; i < letterInfo.Count; i++)
                {
                    var pyedt = letterInfo[i][0].PlanYE.Split('/');
                    int yearMonth1 = Convert.ToInt16(pyedt[0]);
                    int yearDay1 = Convert.ToInt16(pyedt[1]);

                    DateTime newdt = DateTime.UtcNow;
                    newdt = new DateTime(newdt.Year, yearMonth1, yearDay1);
                    dt.Add(newdt);
                }

                DateTime mindt = dt.Min(x => x);

                //var month = letterInfo[0][0].PlanYE.Split('/');
                //int yearMonth = Convert.ToInt16(month[0]);
                //int yearDay = Convert.ToInt16(month[1]);
                int yearMonth = mindt.Month;
                int yearDay = mindt.Day;

                //if (letterInfo[0][0].PrimaryContact)
                //{
                for (int i = 0; i < letterInfo.Count; i++)
                {
                    filterLetterInfo.Add(letterInfo[i][0]);
                }
                //}
                //else 
                if (letterInfo[0][0].Contact_Type == 4)
                {
                    filterLetterInfo.Add(letterInfo[0][0]);
                }

                string str = string.Empty;
                string Plans = string.Empty;
                string PlanNumber = string.Empty;
                // string directoryPath = hostPath + "Files\\FolllowUpLetters/" + filterLetterInfo[0].Client_Name.Replace(' ', '_').Replace('\'', '_').Replace('&', '_').Replace(',', '_') + date + "/";
                string docName = "FollowUP" + letterInfo[0][0].Client_Name.Replace(' ', '_').Replace('\'', '_').Replace('&', '_').Replace(',', '_') + ".docx";

                // string sign = SignaturePicture(filterLetterInfo[0].CSR, pathImage + "Signatures\\");
                string sign = SignaturePicture(filterLetterInfo[0].CSR, pathImage + "Content//ContactImages//");
                // string sign1 = filterLetterInfo[0].SignaturePath;
                // string sign = SignaturePicture(filterLetterInfo[0].CSR, pathImage + "Content//ContactImages//");

                string imgf = "<img src=" + sign + "  height='120px' width='120px'>";

                if (filterLetterInfo[0].MI.Length == 0)
                {
                    obj.MI = "";
                    // htmlDoctemp = htmlDoctemp.Replace("[MI]", "");
                }
                if (filterLetterInfo[0].MI.Length > 0)
                {
                    obj.MI = filterLetterInfo[0].MI;
                    // htmlDoctemp = htmlDoctemp.Replace("[MI]", " " + filterLetterInfo[0].MI);
                }
                if (sign != string.Empty)
                {
                    obj.signature = sign;
                    //htmlDoctemp = htmlDoctemp.Replace("[Signature]", sign);
                }
                else
                {
                    obj.signature = "<p>&nbsp;</p><p>&nbsp;</p>";

                }
                if (filterLetterInfo.Count > 0)
                {
                    for (int i = 0; i < filterLetterInfo.Count; i++)
                    {
                        if (i == 0)
                        {
                            Plans += filterLetterInfo[i].Plan_Name;
                            PlanNumber += filterLetterInfo[i].PlanNumber;
                        }
                        else
                        {
                            Plans += "<p style='margin:0;padding:0;margin-left:40px;'>" + filterLetterInfo[i].Plan_Name + "</p>";
                            PlanNumber += "<p style='margin:0;padding:0;margin-left:40px;'>" + filterLetterInfo[i].PlanNumber + "</p>";
                        }
                    }
                }
                obj.planName = Plans;
                obj.PlanNumber = PlanNumber;

                var cnt = 0;
                for (int i = 0; i < cCInfo.Count; i++)
                {
                    string str1 = string.Empty;
                    int nameCount = 0;
                    if (i != 0)
                    {
                        for (int j = 0; j < i; j++)
                        {
                            string name1 = cCInfo[i].First_Name + " " + cCInfo[i].Last_Name;
                            string name2 = cCInfo[j].First_Name + " " + cCInfo[j].Last_Name;
                            if (name1 == name2)
                            {
                                nameCount++;
                                continue;
                            }
                        }
                    }
                    if (nameCount == 0)
                    {
                        if (cCInfo[i].First_Name != "" && cCInfo[i].Last_Name != "")
                        {


                            str1 += cCInfo[i].First_Name;
                            if (cCInfo[i].MI.Length > 0)
                            {
                                str1 += ' ' + cCInfo[i].MI;
                            }

                            str1 += ' ' + cCInfo[i].Last_Name + cCInfo[i].Enclosures;
                            if (cCInfo[i].Via != "Mail" && cCInfo[i].Via.Length > 0)
                            {

                                str1 += ' ' + cCInfo[i].Via + ',';
                            }

                        }

                        cnt++;
                    }
                    //str = str.TrimEnd(',');
                    str += "<p style='margin:0;padding:0;'>" + str1 + "</p>";
                }
                //  htmlDoctemp = htmlDoctemp.Replace("[planName]", Plans);
                //  htmlDoctemp = htmlDoctemp.Replace("[ccInformation]", str);
                obj.ccInformation = str;

                //Author:Chayan Paria
                //Date:17/1/2017
                //Append Checklist string
                if (isblank == true)
                {
                    htmlDoctemp += FollowUpcheckList(letterInfo, followUpInfo, pYEYear);
                }

                htmlDoctemp = "<div style='font-family:calibri;'>" + htmlDoctemp + "</div>";

                return htmlDoctemp.ToString();
            }
            catch (Exception ex)
            {
                throw (ex);
            }


        }

        //        /// <summary>
        //        /// Generate followup letter2
        //        /// </summary>
        //        /// <param name="letterInfo"></param>
        //        /// <param name="cCInfo"></param>
        //        /// <param name="followUpInfo"></param>
        //        /// <param name="obj"></param>
        //        /// <returns></returns>
        //        public static string GenrateFollowupLetter2(
        //        List<List<LetterInfoModel>> letterInfo,
        //        List<LetterCCInfoModel> cCInfo,
        //        List<List<FollowUpLetterModel>> followUpInfo,
        //        int pYEYear,
        //      string userInitials, string hostPath, string htmlDoc, string dateOfRecord, int UID, string FileName, string isblank, CommModule_AllPredefinedKey obj)
        //        {

        //            string htmlDoctemp = htmlDoc;
        //            try
        //            {
        //                var date = (DateTime.UtcNow.Date).ToString("dd-MM-yyyy");
        //               // var dtfi = new DateTimeFormatInfo { ShortDatePattern = "MM/dd/yyyy", DateSeparator = "/" };
        //               // DateTime dtOfRecord = Convert.ToDateTime(dateOfRecord, dtfi);
        //                DateTime dtOfRecord = DateTime.UtcNow.Date;
        //                var filterLetterInfo = new List<LetterInfoModel>();
        //                var month = letterInfo[0][0].PlanYE.Split('/');
        //                int yearMonth = Convert.ToInt16(month[0]);
        //                int yearDay = Convert.ToInt16(month[1]);
        //                if (letterInfo[0][0].PrimaryContact)
        //                {
        //                    for (int i = 0; i < letterInfo.Count; i++)
        //                    {
        //                        filterLetterInfo.Add(letterInfo[i][0]);
        //                    }
        //                }
        //                else if (letterInfo[0][0].Contact_Type == 4)
        //                {
        //                    filterLetterInfo.Add(letterInfo[0][0]);
        //                }

        //                string str = string.Empty;
        //                string Plans = string.Empty;
        //             //   string directoryPath = hostPath + "Files\\FolllowUpLetters/" + filterLetterInfo[0].Client_Name.Replace(' ', '_').Replace('\'', '_').Replace('&', '_').Replace(',', '_') + date + "/";
        //                string docName = "FollowUP" + letterInfo[0][0].Client_Name.Replace(' ', '_').Replace('\'', '_').Replace('&', '_').Replace(',', '_') + ".docx";


        //                string sign = SignaturePicture(filterLetterInfo[0].CSR, pathImage + "Signatures\\");

        //                if (sign != string.Empty)
        //                {
        //                    obj.signature = sign;
        //                    //htmlDoctemp = htmlDoctemp.Replace("[Signature]", sign);
        //                }
        //                else
        //                {
        //                  obj.signature = "<p>&nbsp;</p><p>&nbsp;</p>";
        //                }
        //                if (filterLetterInfo[0].MI.Length > 0)
        //                {
        //                    obj.MI = filterLetterInfo[0].MI;
        //                    //htmlDoctemp = htmlDoctemp.Replace("[MI]", " " + filterLetterInfo[0].MI);
        //                }
        //                else
        //                {
        //                    obj.MI = "";
        //                    //htmlDoctemp = htmlDoctemp.Replace("[MI]", "");
        //                }
        //                if (filterLetterInfo.Count > 0)
        //                {
        //                    for (int i = 0; i < filterLetterInfo.Count; i++)
        //                    {
        //                        if (i == 0)
        //                        {
        //                            Plans += filterLetterInfo[i].Plan_Name ;
        //                        }
        //                        else
        //                        {
        //                            Plans += "<p>" + filterLetterInfo[i].Plan_Name + "</p>";
        //                        }
        //                    }
        //                }
        //                var cnt = 0;
        //                for (int i = 0; i < cCInfo.Count; i++)
        //                {
        //                    int nameCount = 0;
        //                    if (i != 0)
        //                    {
        //                        for (int j = 0; j < i; j++)
        //                        {
        //                            string name1 = cCInfo[i].First_Name + " " + cCInfo[i].Last_Name;
        //                            string name2 = cCInfo[j].First_Name + " " + cCInfo[j].Last_Name;
        //                            if (name1 == name2)
        //                            {
        //                                nameCount++;
        //                                continue;
        //                            }
        //                        }
        //                    }
        //                    if (nameCount == 0)
        //                    {
        //                        if (cCInfo[i].First_Name != "" && cCInfo[i].Last_Name != "")
        //                        {


        //                            str += cCInfo[i].First_Name;
        //                            if (cCInfo[i].MI.Length > 0)
        //                            {
        //                                str += ' ' + cCInfo[i].MI;
        //                            }

        //                            str += ' ' + cCInfo[i].Last_Name + cCInfo[i].Enclosures;
        //                            if (cCInfo[i].Via != "Mail" && cCInfo[i].Via.Length > 0)
        //                            {

        //                                str += ' ' + cCInfo[i].Via + ',';
        //                            }

        //                        }

        //                        cnt++;
        //                    }
        //                    str = str.TrimEnd(',');
        //                }

        //                //htmlDoctemp = htmlDoctemp.Replace("[ccInformation]", str);
        //                //htmlDoctemp = htmlDoctemp.Replace("[planName]", Plans);
        //                obj.planName = Plans;
        //                obj.ccInformation = str;

        //                //Author:Chayan Paria
        //                //Date:17/1/2017
        //                //Append Checklist string
        //                //if (isblank == "False")
        //                //{
        //                    htmlDoctemp += FollowUpcheckList(letterInfo, followUpInfo, pYEYear);

        //                //}


        //                htmlDoctemp = "<div style='font-family:calibri;'>" + htmlDoctemp + "</div>";
        //                return htmlDoctemp.ToString();
        //            }
        //            catch (Exception ex)
        //            {
        //                throw (ex);
        //            }
        //        }

        //        /// <summary>
        //        /// Generate followup letter3
        //        /// </summary>
        //        /// <param name="letterInfo"></param>
        //        /// <param name="cCInfo"></param>
        //        /// <param name="followUpInfo"></param>
        //        /// <param name="obj"></param>
        //        /// <returns></returns>
        //        public static string GenrateFollowupLetter3(
        //     List<List<LetterInfoModel>> letterInfo,
        //     List<LetterCCInfoModel> cCInfo,
        //     List<List<FollowUpLetterModel>> followUpInfo,
        //     int pYEYear,
        //   string userInitials, string hostPath, string htmlDoc, string dateOfRecord, int UID, string FileName, string isblank,CommModule_AllPredefinedKey obj)
        //        {

        //            string htmlDoctemp = htmlDoc;
        //            try
        //            {
        //                int dbcount = 0;
        //                var date = (DateTime.UtcNow.Date).ToString("dd-MM-yyyy");
        //               // var dtfi = new DateTimeFormatInfo { ShortDatePattern = "MM/dd/yyyy", DateSeparator = "/" };
        //               // DateTime dtOfRecord = Convert.ToDateTime(dateOfRecord, dtfi);
        //                DateTime dtOfRecord = DateTime.UtcNow.Date;
        //                var filterLetterInfo = new List<LetterInfoModel>();
        //                var month = letterInfo[0][0].PlanYE.Split('/');
        //                int yearMonth = Convert.ToInt16(month[0]);
        //                int yearDay = Convert.ToInt16(month[1]);
        //                if (letterInfo[0][0].PrimaryContact)
        //                {
        //                    for (int i = 0; i < letterInfo.Count; i++)
        //                    {
        //                        filterLetterInfo.Add(letterInfo[i][0]);
        //                    }
        //                }

        //                else if (letterInfo[0][0].Contact_Type == 4)
        //                {
        //                    filterLetterInfo.Add(letterInfo[0][0]);
        //                }

        //                for (int i = 0; i < filterLetterInfo.Count; i++)
        //                {
        //                    if (filterLetterInfo[i].Plan_Type_Code == 4)
        //                    {
        //                        dbcount++;
        //                    }

        //                }
        //                var pyeDate = new DateTime(pYEYear, yearMonth, yearDay);
        //                DateTime rushStart = pyeDate.AddMonths(7);
        //                rushStart = new DateTime(rushStart.Year, rushStart.Month, 15);

        //                DateTime unExtDate = pyeDate.AddMonths(7);

        //                DateTime rushEnd = pyeDate.AddMonths(8);
        //                rushEnd = new DateTime(rushEnd.Year, rushEnd.Month, 15);

        //                DateTime extendedDate = pyeDate.AddMonths(10);
        //                extendedDate = new DateTime(extendedDate.Year, extendedDate.Month, 15);

        //                //For Extended Contribution Date
        //                DateTime contributionDate = pyeDate.AddMonths(9);
        //                contributionDate = new DateTime(contributionDate.Year, contributionDate.Month, 15);
        //                //For Funding Deadline
        //                //DateTime fundingDeadline = pyeDate.AddMonths(3);
        //                //fundingDeadline = new DateTime(fundingDeadline.Year, fundingDeadline.Month, 15);

        //                DateTime minimumFundingDeadLine = pyeDate.AddMonths(9);
        //                minimumFundingDeadLine = new DateTime(contributionDate.Year, contributionDate.Month, 15);

        //                string str = string.Empty;
        //                string Plans = string.Empty;


        //                string sign = SignaturePicture(filterLetterInfo[0].CSR, pathImage + "Signatures\\");

        //                if (sign != string.Empty)
        //                {
        //                    obj.signature = sign;
        //                   // htmlDoctemp = htmlDoctemp.Replace("[Signature]", sign);
        //                }
        //                else
        //                {
        //                    obj.signature = "<p>&nbsp;</p><p>&nbsp;</p>";
        //                }
        //                if (filterLetterInfo[0].MI.Length > 0)
        //                {
        //                    obj.MI = filterLetterInfo[0].MI;
        //                    //htmlDoctemp = htmlDoctemp.Replace("[MI]", " " + filterLetterInfo[0].MI);
        //                }
        //                else
        //                {
        //                    obj.MI = "";
        //                  //  htmlDoctemp = htmlDoctemp.Replace("[MI]", "");
        //                }



        //                if (filterLetterInfo.Count > 0)
        //                {
        //                    for (int i = 0; i < filterLetterInfo.Count; i++)
        //                    {
        //                        if (i == 0)
        //                        {
        //                            Plans = "<p>" + filterLetterInfo[i].Plan_Name + "</p>";
        //                        }
        //                        else
        //                        {
        //                            Plans += "<p style='margin:0;padding:0;margin-left:40px'>" + filterLetterInfo[i].Plan_Name + "</p>";

        //                        }
        //                    }
        //                }
        //                var cnt = 0;
        //                for (int i = 0; i < cCInfo.Count; i++)
        //                {
        //                    int nameCount = 0;
        //                    if (i != 0)
        //                    {
        //                        for (int j = 0; j < i; j++)
        //                        {
        //                            string name1 = cCInfo[i].First_Name + " " + cCInfo[i].Last_Name;
        //                            string name2 = cCInfo[j].First_Name + " " + cCInfo[j].Last_Name;
        //                            if (name1 == name2)
        //                            {
        //                                nameCount++;
        //                                continue;
        //                            }
        //                        }
        //                    }
        //                    if (nameCount == 0)
        //                    {
        //                        if (cCInfo[i].First_Name != "" && cCInfo[i].Last_Name != "")
        //                        {


        //                            str += cCInfo[i].First_Name;
        //                            if (cCInfo[i].MI.Length > 0)
        //                            {
        //                                str += ' ' + cCInfo[i].MI;
        //                            }

        //                            str += ' ' + cCInfo[i].Last_Name + cCInfo[i].Enclosures;
        //                            if (cCInfo[i].Via != "Mail" && cCInfo[i].Via.Length > 0)
        //                            {

        //                                str += ' ' + cCInfo[i].Via + ',';
        //                            }

        //                        }

        //                        cnt++;
        //                    }
        //                    str = str.TrimEnd(',');
        //                }
        //               // htmlDoctemp = htmlDoctemp.Replace("[planName]", Plans);
        //               // htmlDoctemp = htmlDoctemp.Replace("[ccInformation]", str);

        //                obj.planName = Plans;
        //                obj.ccInformation = str;

        //                //Author:Chayan Paria
        //                //Date:17/1/2017
        //                //Append Checklist string
        //                //if (isblank == "False")
        //                //{
        //                    htmlDoctemp += FollowUpcheckList(letterInfo, followUpInfo, pYEYear);

        //                //}


        //                htmlDoctemp = "<div style='font-family:calibri;'>" + htmlDoctemp + "</div>";
        //                return htmlDoctemp.ToString();
        //            }
        //            catch (Exception ex)
        //            {
        //                throw (ex);
        //            }
        //        }

        /// <summary>
        /// Get followup Plan Details
        /// </summary>
        /// <param name="letterInfo"></param>
        /// <param name="followUpInfo"></param>
        /// <param name="pYEYear"></param>

        public static string FollowUpcheckList(List<List<LetterInfoModel>> letterInfo, List<List<FollowUpLetterModel>> followUpInfo, int pYEYear)
        {
            try
            {

                var filterLetterInfo = new List<LetterInfoModel>();
                var month = letterInfo[0][0].PlanYE.Split('/');
                int yearMonth = Convert.ToInt16(month[0]);
                int yearDay = Convert.ToInt16(month[1]);

                int censusCount = 0;
                int auCount = 0;
                for (int i = 0; i < letterInfo.Count; i++)
                {
                    var j = i + 1;
                    for (int k = 0; k < followUpInfo[j].Count; k++)
                    {
                        if (followUpInfo[j][k].TrackingDocText.ToLower().IndexOf("census", StringComparison.Ordinal) > -1)
                        {
                            censusCount++;
                        }

                        if (followUpInfo[j][k].TrackingDocText.ToLower().IndexOf("business info update", StringComparison.Ordinal) > -1)
                        {
                            auCount++;
                        }
                    }
                }

                //<p style="font-size: 14px;text-align: left;"><strong><b><u>PERSONAL AND CONFIDENTIAL</u></b></strong></p>
                StringBuilder builder = new StringBuilder();
                builder.Append("<div class='container'>");

                //builder.Append("<p style='font-family:calibri;font-size:22px;'><b>CHECK LIST</b></p>");
                builder.Append("<p style='font-family:calibri;font-size:20px;'><b>CHECK LIST</b></p>");
                builder.Append("<p style='font-family:calibri;font-size:18px;text-align: justify;'>The items listed below are needed to complete annual administration for your plan(s). <b>Please Note: </b>Details related to the information requested below can be found in your ShareFile folder at <a href='https://boycepensions.sharefile.com' target='_blank'>https://boycepensions.sharefile.com</a> (your email address is your username). </p>");
                //builder.Append("<p> &nbsp</p>");
                string bulet = "\u274F";
                builder.Append("<p style='font-family:calibri;font-size:20px;'><b><i>YOUR COMPANY INFORMATION </i></b></p>");
                builder.Append("<p style='font-family:calibri;font-size:18px;'><b><i>" + letterInfo[0][0].Client_Name + " </i></b></p>");
                builder.Append("<ul style='font-family:calibri;font-size:15px;text-align:justify;list-style-type:none;'>");
                if (censusCount > 0)
                {
                    // builder.Append("<li> " + uncheckChkImg + " Census  </li>");
                    builder.Append("<li> " + bulet + " Census  </li>");
                }
                if (auCount > 0)
                {
                    builder.Append("<li> " + bulet + " Business Info Update  </li>");
                }
                builder.Append("</ul>");

                builder.Append("<p style='font-family:calibri;font-size:20px;'><b><i>YOUR PLAN INFORMATION </i></b></p>");

                for (int i = 0; i < letterInfo.Count; i++)
                {
                    int planNo = i + 1;
                    builder.Append("<div style='font-family:calibri;font-size:20px;'><b>Plan #" + planNo + ": " + letterInfo[i][0].Plan_Name + "</b></div>");
                    builder.Append("<ul style='font-family:calibri;font-size:15px;text-align:justify;list-style-type:none;'>");

                    List<FollowUpLetterModel> assetData = new List<FollowUpLetterModel>();
                    List<FollowUpLetterModel> schAData = new List<FollowUpLetterModel>();//added list for counting schedule a assets by nikita
                    List<FollowUpLetterModel> mvData = new List<FollowUpLetterModel>();//Added list for counting Market Valuation Assets by chandrashekhar

                    int j = i + 1;
                    for (int k = 0; k < followUpInfo[j].Count; k++)
                    {
                        if (followUpInfo[j][k].TrackingDocText.ToLower().IndexOf("audit", StringComparison.Ordinal) > -1)
                        {
                            continue;
                        }

                        if (followUpInfo[j][k].TrackingDocText.ToLower().IndexOf("fee estimate", StringComparison.Ordinal) > -1)
                        {
                            continue;
                        }

                        if (followUpInfo[j][k].TrackingDocText.ToLower().IndexOf("client paid", StringComparison.Ordinal) > -1)
                        {
                            continue;
                        }

                        if (followUpInfo[j][k].TrackingDocText.ToLower().IndexOf("fidelity bond", StringComparison.Ordinal) > -1)
                        {
                            continue;
                        }

                        if (followUpInfo[j][k].TrackingDocText.ToLower().IndexOf("annual update", StringComparison.Ordinal) > -1)
                        {
                            continue;
                        }

                        if (followUpInfo[j][k].TrackingDocText.ToLower().IndexOf("business info update", StringComparison.Ordinal) > -1)
                        {
                            continue;
                        }

                        if (followUpInfo[j][k].TrackingDocText.ToLower().IndexOf("census", StringComparison.Ordinal) > -1)
                        {
                            continue;
                        }
                        if (followUpInfo[j][k].TrackingDocText.ToLower().IndexOf("schedule a", StringComparison.Ordinal) > -1 && followUpInfo[j][k].TrackingDoc_Type_Add == -1)
                        {
                            continue;
                        }
                        if (followUpInfo[j][k].TrackingDocText.ToLower().IndexOf("schedule a", StringComparison.Ordinal) > -1 && followUpInfo[j][k].TrackingDoc_Type_Add != -1)
                        {
                            schAData.Add(followUpInfo[j][k]);
                            continue;
                        }
                        //Added by Chandrashekhar for Market Valuation
                        if (followUpInfo[j][k].TrackingDocText.ToLower().IndexOf("market valuation", StringComparison.Ordinal) > -1 && followUpInfo[j][k].TrackingDoc_Type_Add == -1)
                        {
                            continue;
                        }
                        //Added by Chandrashekhar for Market Valuation
                        if (followUpInfo[j][k].TrackingDocText.ToLower().IndexOf("market valuation", StringComparison.Ordinal) > -1 && followUpInfo[j][k].TrackingDoc_Type_Add != -1 && followUpInfo[j][k].PYE_Year >= 2016)
                        {
                            mvData.Add(followUpInfo[j][k]);
                            continue;
                        }
                        if (followUpInfo[j][k].TrackingDocText.ToLower().IndexOf("asset", StringComparison.Ordinal) > -1)
                        {
                            assetData.Add(followUpInfo[j][k]);
                            continue;
                        }

                        builder.Append("<li> " + bulet + " " + followUpInfo[j][k].TrackingDocText + "</li>");
                    }
                    SqlConnection con = new SqlConnection(dbCon);
                    //If schedule a count > 0 then
                    if (schAData.Count > 0)
                    {
                        builder.Append("<li> " + bulet + " Schedule A </li>");
                        foreach (FollowUpLetterModel followUpLetter in schAData)
                        {
                            cmdStr = "Select Top 1  InvestmentPlatform_Text from InvestmentPlatforms_Lib where InvestmentPlatform_ID=" + followUpLetter.InvestmentPlatform + " ";
                            dt = ExecuteSqlCommand(cmdStr, con);

                            List<FollowupsValue> statementFromText = ConvertDataTableToList<FollowupsValue>(dt);
                            // string statementFromText = (from x in entity.InvestmentPlatforms_Lib where x.InvestmentPlatform_ID == followUpLetter.InvestmentPlatform select x.InvestmentPlatform_Text).FirstOrDefault();
                            builder.Append("<li style='margin-top: 1px;margin-left:20px;'>" + bulet + " " + statementFromText[0].InvestmentPlatform_Text + ":" + followUpLetter.RecordKeeperText + ":" + HideAcctNum(followUpLetter.RKAccountNoText) + " </li>");
                        }
                    }

                    //Added by Chandrashekhar for Market Valuation Tracking
                    if (mvData.Count > 0)
                    {
                        builder.Append("<li> " + bulet + " Market Valuation </li>");
                        foreach (FollowUpLetterModel followUpLetter in mvData)
                        {
                            cmdStr = "Select Top 1  InvestmentPlatform_Text from InvestmentPlatforms_Lib where InvestmentPlatform_ID=" + followUpLetter.InvestmentPlatform + " ";
                            dt = ExecuteSqlCommand(cmdStr, con);

                            List<FollowupsValue> statementFromText = ConvertDataTableToList<FollowupsValue>(dt);
                            // string statementFromText = (from x in entity.InvestmentPlatforms_Lib where x.InvestmentPlatform_ID == followUpLetter.InvestmentPlatform select x.InvestmentPlatform_Text).FirstOrDefault();
                            builder.Append("<li style='margin-left:20px;margin-top: 1px;'>" + bulet + " " + statementFromText[0].InvestmentPlatform_Text + ":" + followUpLetter.RecordKeeperText + ":" + HideAcctNum(followUpLetter.RKAccountNoText) + " </li>");
                        }

                    }
                    //End of Market Valuation

                    //Asset statement
                    if (assetData.Count > 0)
                    {
                        builder.Append("<li> " + bulet + " Asset Statements: </li>");

                        foreach (FollowUpLetterModel followUpLetter in assetData)
                        {
                            string assetText = string.Empty;

                            if (followUpLetter.RecordKeeperText.Length > 0 && followUpLetter.RecordKeeperText.ToLower() != "all")
                            {
                                assetText += followUpLetter.RecordKeeperText;
                            }
                            if (followUpLetter.RKAccountNoText.Length > 0 && followUpLetter.RKAccountNoText.ToLower() != "all")
                            {
                                assetText += " (# " + HideAcctNum(followUpLetter.RKAccountNoText) + ")";
                            }
                            if (followUpLetter.CustodianText.Length > 0 && followUpLetter.CustodianText.ToLower() != "all")
                            {
                                assetText += " " + followUpLetter.CustodianText;
                            }
                            if (followUpLetter.CAccountNoText.Length > 0 && followUpLetter.CAccountNoText.ToLower() != "all")
                            {
                                assetText += " (# " + HideAcctNum(followUpLetter.CAccountNoText) + ")";
                            }

                            if (followUpLetter.Month01 > 0)
                            {
                                var months = new ArrayList();
                                var rcvd = new ArrayList();

                                months.Add(followUpLetter.Month01);
                                rcvd.Add(followUpLetter.Received01);
                                months.Add(followUpLetter.Month02);
                                rcvd.Add(followUpLetter.Received02);
                                months.Add(followUpLetter.Month03);
                                rcvd.Add(followUpLetter.Received03);
                                months.Add(followUpLetter.Month04);
                                rcvd.Add(followUpLetter.Received04);
                                months.Add(followUpLetter.Month05);
                                rcvd.Add(followUpLetter.Received05);
                                months.Add(followUpLetter.Month06);
                                rcvd.Add(followUpLetter.Received06);
                                months.Add(followUpLetter.Month07);
                                rcvd.Add(followUpLetter.Received07);
                                months.Add(followUpLetter.Month08);
                                rcvd.Add(followUpLetter.Received08);
                                months.Add(followUpLetter.Month09);
                                rcvd.Add(followUpLetter.Received09);
                                months.Add(followUpLetter.Month10);
                                rcvd.Add(followUpLetter.Received10);
                                months.Add(followUpLetter.Month11);
                                rcvd.Add(followUpLetter.Received11);
                                months.Add(followUpLetter.Month12);
                                rcvd.Add(followUpLetter.Received12);

                                var statementStr = string.Empty;
                                var firstDate = string.Empty;

                                int statementYear;
                                if (Convert.ToInt16(months[0]) > 1)
                                {
                                    statementYear = pYEYear - 1;
                                }
                                else
                                {
                                    statementYear = pYEYear;
                                }

                                var x = 0;
                                while (x < 12)
                                {
                                    while (x < 12 && Convert.ToBoolean(rcvd[x]))
                                    {
                                        if (x > 0 && x < 12 && Convert.ToInt16(months[x]) == 1)
                                        {
                                            statementYear++;
                                        }

                                        x++;
                                    }

                                    if (x < 12 && !Convert.ToBoolean(rcvd[x]))
                                    {
                                        firstDate =
                                            new DateTime(statementYear, Convert.ToInt16(months[x]), 1).ToString("MMMM yyyy");
                                    }

                                    while (x < 12 && !Convert.ToBoolean(rcvd[x]))
                                    {
                                        if (x > 0 && Convert.ToInt16(months[x]) == 1)
                                        {
                                            statementYear++;
                                        }

                                        x++;
                                    }

                                    if (x < 12 && (x <= 0 || Convert.ToBoolean(rcvd[x - 1])))
                                    {
                                        continue;
                                    }

                                    if (x > 12 || Convert.ToBoolean(rcvd[x - 1]))
                                    {
                                        continue;
                                    }

                                    if (statementStr.Length > 0)
                                    {
                                        statementStr += ",";
                                    }

                                    // SecondDate = statemntfmt.format(new Date(StatementYear, int(Months[x-1])-1, 1));
                                    var secondDate = new DateTime(statementYear, Convert.ToInt16(months[x - 1]), 1).ToString(
                                        "MMMM yyyy");
                                    if (firstDate != secondDate)
                                    {
                                        statementStr += firstDate + "-" + secondDate;
                                    }
                                    else
                                    {
                                        statementStr += firstDate;
                                    }

                                    firstDate = string.Empty;
                                }

                                if (statementStr.Length > 0 && followUpLetter.StatementDelivery != 3)
                                {
                                    assetText += " for the following months: " + statementStr;
                                }
                                if (followUpLetter.StatementDelivery == 3)
                                {
                                    assetText += " Income Statement & Balance Sheet ";
                                }
                            }
                            builder.Append("<li style='margin-top: 1px;margin-left:20px;'>" + bulet + " " + assetText + " </li>");
                        }
                    }
                    builder.Append("</ul>");
                }

                builder.Append("</div>");

                return builder.ToString();
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Get Signature Picture
        /// </summary>
        public static string SignaturePicture(int csr, string hostPath)
        {
            try
            {
                var sign = string.Empty;

                switch (csr)
                {
                    case 19:
                        sign = hostPath + "19.png";
                        break;

                    case 26:
                        sign = hostPath + "26.png";
                        break;

                    case 27:
                        sign = hostPath + "27.png";
                        break;

                    case 28:
                        sign = hostPath + "28.png";
                        break;

                    case 30:
                        sign = hostPath + "30.png";
                        break;

                    case 41:
                        sign = hostPath + "41.png";
                        break;

                    case 47:
                        sign = hostPath + "47.png";
                        break;

                    case 48:
                        sign = hostPath + "48.png";
                        break;

                    case 49:
                        sign = hostPath + "49.png";
                        break;

                    case 51:
                        sign = hostPath + "51.png";
                        break;

                    case 53:
                        sign = hostPath + "53.png";
                        break;

                    case 56:
                        sign = hostPath + "56.png";
                        break;

                    case 61:
                        sign = hostPath + "61.png";
                        break;

                    case 67:
                        sign = hostPath + "67.png";
                        break;
                }

                return sign;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// get Generic LetterCCInfo value
        /// </summary>
        public static List<LetterCCInfoModel> getGenericLetterCCInfo(int Client_Id, int Plan_Number, string TokenID, int ConnectionIndex)
        {
            try
            {
                SqlConnection con = new SqlConnection(dbCon);
                string sqlCmd =
                            "Select ct.First_Name, ct.Last_Name, ct.MI, ct.[Credentials], ct.Client_ID, \r\n" +
                            "Case ct.CC When 2 Then '(without enclosures)' When 3 Then '(with enclosures)' Else '' End as Enclosures, \r\n" +
                            "IsNull((Select 'Via ' + CommunicationType_Text From CommunicationType com Where com.CommunicationType_ID = ct.CommunicationPreference),'') as Via, \r\n" +
                            "0 as Plan_Number, Contact_Method.Email \r\n" +
                            "From Contacts ct left join Contact_Method on ct.Contact_Method=Contact_Method.Contact_Method_ID Where CC > 1 and ct.IsPortalDeleted=0 \r\n";
                if (Client_Id > -1)
                    sqlCmd += " and ct.Client_ID = " + Client_Id + " \r\n";


                sqlCmd += "Union \r\n" +
                          "Select Advisor.First_Name, Advisor.Last_Name, Advisor.MI, Advisor.[Credentials], Advisor_Client_Link.Client_ID, \r\n" +
                          "Case Advisor_Client_Link.CC When 2 Then '(without enclosures)' When 3 Then '(with enclosures)' Else '' End as Enclosures, \r\n" +
                          "IsNull((Select 'Via ' + CommunicationType_Text From CommunicationType com Where com.CommunicationType_ID = Advisor.CommunicationPreference),'') as Via, \r\n" +
                          "Advisor_Client_Link.Plan_Number, Contact_Method.Email \r\n" +
                          "From Advisor Left Join Advisor_Client_Link on Advisor.Advisor_ID = Advisor_Client_Link.Advisor_ID \r\n" +
                          "left join Contact_Method on Advisor.Contact_Method=Contact_Method.Contact_Method_ID \r\n" +
                          "Where Advisor_Client_Link.CC > 1 \r\n";
                if (Client_Id > -1)
                    sqlCmd += " and Advisor_Client_Link.Client_ID = " + Client_Id + " ";
                if (Plan_Number > -1)
                    sqlCmd += " and Advisor_Client_Link.Plan_Number = " + Plan_Number + " ";
                // List<LetterCCInfoModel> data = entity.ExecuteStoreQuery<LetterCCInfoModel>(sqlCmd).ToList();
                dt = ExecuteSqlCommand(sqlCmd, con);
                List<LetterCCInfoModel> data = ConvertDataTableToList<LetterCCInfoModel>(dt);

                return data;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// get Generic LetterInfo
        /// </summary>
        public static List<LetterInfoModel> getGenericLetterInfo(int Client_Id, int Plan_Number, string TokenID, int ConnectionIndex, int Contact_Id)
        {
            try
            {
                SqlConnection con = new SqlConnection(dbCon);
                string cmd = "Select c.Client_ID, c.Client_Name, c.Segmentation, c.Business_Type, p.Plan_Number,  \r\n" +
                            "ct.First_Name, ct.Contact_ID, ct.Last_Name, ct.MI, ct.Title, ct.[Credentials], \r\n" +
                            "(Case ct.Greeting When '' Then ct.First_Name Else ct.Greeting End) as Greeting_Text, ad.Address1, ad.Address2, ad.City,  \r\n" +
                            "s.State_Abrev, ad.Zip_Code,adphy.Address1 as PhysicalAddress1, adphy.Address2 as PhysicalAddress2, adphy.City as PhysicalCity,sphy.State_Abrev as PhysicalState_abv ,adphy.Zip_Code as PhysicalZipCode,cm.Business_Phone, cm.Phone, cm.Mobile_Phone, cm.Fax, cm.Email,  \r\n" +
                            "(case SubString(p.PlanYE,1,2) When '01' Then 'January'  \r\n" +
                            "                              When '02' Then 'Febuary'  \r\n" +
                            "                              When '03' Then 'March'  \r\n" +
                            "                              When '04' Then 'April'  \r\n" +
                            "                              When '05' Then 'May'  \r\n" +
                            "                              When '06' Then 'June'  \r\n" +
                            "                              When '07' Then 'July'  \r\n" +
                            "                              When '08' Then 'August'  \r\n" +
                            "                              When '09' Then 'September'  \r\n" +
                            "                              When '10' Then 'October'  \r\n" +
                            "                              When '11' Then 'November'  \r\n" +
                            "                              When '12' Then 'December'  \r\n" +
                            "end) + ' ' + SubString(p.PlanYE,4,2) as PlanYearEnd,  \r\n" +
                            "(Select Business_Type_Text from Business_Type where Business_Type.Business_Type_ID = c.Business_Type) as BusinessTypeText,  \r\n" +
                            "IsNull((Select Service_Type_Text From Service_Type Where Service_Type_ID = p.ServiceType), 'Not Set') as ServiceType,  \r\n" +
                            "(Select PlanStatusType_Text From PlanStatusType Where PlanStatusType_ID = p.PlanStatus) as PlanStatus,  \r\n" +
                            "(Select DocType_Text From Doc_Types Where Doc_Types.DocType_ID = p.DocType) as DocType, p.Consultant, p.CSR,  \r\n" +
                            "p.Plan_Name, (Right(replicate('0',3) + Convert(varchar(3),Plan_Number),3)) as PlanNumber, p.Plan_Type_Code, p.PlanYE, \r\n" +
                            "ct.Contact_Type, ct.PrimaryContact, p.Plans_Index_ID,  \r\n" +
                            "IsNull((Select Plan_Type_Code_Text from Plan_Type_Code ptc Where ptc.Plan_Type_Code_ID = p.Plan_Type_Code),'Not Set') as PlanTypeText,  \r\n" +
                            "(Select ut.First_Name + ' ' + ut.MI + ' '+ ut.Last_Name From UserTable ut Where ut.SysUserID = Consultant) as ConsultantFull,  \r\n" +
                            "(Select ut.Initials From UserTable ut Where ut.SysUserID = Consultant) as ConsultantInitials,  \r\n" +
                            "(Select ut.Certifications From UserTable ut Where ut.SysUserID = Consultant) as ConsultantCertifications,  \r\n" +
                            "(Select Email From UserTable left join Contact_Method On UserTable.ContactMethod = Contact_Method.Contact_Method_ID Where SysUserID = Consultant) as ConsultantEmail,  \r\n" +
                            "(Select Business_Phone + ' ' + Business_Phone_Ext From UserTable left join Contact_Method On UserTable.ContactMethod = Contact_Method.Contact_Method_ID Where SysUserID = Consultant) as ConsultantPhone,  \r\n" +
                            "(Select ut.First_Name + ' ' + ut.Last_Name From UserTable ut Where ut.SysUserID = CSR) as CSRFull,  \r\n" +
                            "(Select Email From UserTable left join Contact_Method On UserTable.ContactMethod = Contact_Method.Contact_Method_ID Where SysUserID = CSR) as CSREmail,  \r\n" +
                            "(Select Business_Phone + ' ' + Business_Phone_Ext From UserTable left join Contact_Method On UserTable.ContactMethod = Contact_Method.Contact_Method_ID Where SysUserID = CSR) as CSRPhone,  \r\n" +
                            "(Select ut.Title From UserTable ut Where ut.SysUserID = CSR) as CSRTitle, \r\n" +
                            "(Select ut.SignatureId From UserTable ut Where ut.SysUserID = CSR) as SignatureId,  \r\n" +
                            "(Select Top 1 utSig.Local_Name From Portal_UserSignatures utSig Where utSig.SignatureId = SignatureId) as SignaturePath \r\n" +
                            "from Client_Master c Left Join Plans p on c.Client_ID = p.Client_ID  \r\n" +
                            "Left Join Contacts ct on c.Client_ID = ct.Client_ID  \r\n" +
                            "Left Join [Address] ad on ct.MailAddress = ad.Address_ID  \r\n" +
                            "Left Join [Address] adphy on ct.Address = adphy.Address_ID  \r\n" +
                            "Left Join States s on ad.State = s.States_ID  \r\n" +
                            "Left Join States sphy on adphy.State = sphy.States_ID  \r\n" +
                            "Left Join Contact_Method cm on ct.Contact_Method = cm.Contact_Method_ID  \r\n" +
                            "Where p.Plan_Number IS NOT NULL AND ( ct.PrimaryContact = 1  \r\n" +
                            "or (Select contt.Contact_Type_ID From Contact_Types contt Where contt.Contact_Type_ID in  \r\n" +
                            "(Select li.LookupIndex_ID From LookupIndex li Where li.Value & ct.Contact_Type = li.Value and li.LookupIndex_ID = 7)) = 7  \r\n" +
                            ") AND  \r\n" +
                            "c.IsDead = 0 \r\n";


                if (Client_Id > -1)
                    cmd += " AND c.Client_ID = " + Client_Id + " ";
                if (Plan_Number > -1)
                    cmd += " AND p.Plan_Number = " + Plan_Number + " ";
                if (Plan_Number > -1)
                    cmd += " AND ct.Contact_ID = " + Contact_Id + " ";


                //List<LetterInfoModel> data = entity.ExecuteStoreQuery<LetterInfoModel>(cmd).ToList();

                dt = ExecuteSqlCommand(cmd, con);
                List<LetterInfoModel> data = ConvertDataTableToList<LetterInfoModel>(dt);
                return data;
            }
            catch (Exception)
            {
                throw;
            }

        }


        /// <summary>
        /// Get Follow Up LetterInfo
        /// </summary>
        public static List<FollowUpLetterModel> getFollowUpLetterInfo(int plans_Index_ID, int pYE_Year, string TokenID, int ConnectionIndex)
        {
            SqlConnection con = new SqlConnection(dbCon);
            string sqlCmd =
                        "Select distinct \r\n" +
                        "   (Select TrackingDoc_Name From TrackingDoc_Type Where TrackingDoc_Type_ID = pye.TrackingDoc_Type_ID) as TrackingDocText, \r\n" +
                        "   pye.TrackingDoc_Type_Add, pye.Plans_Index_ID, \r\n" +
                        "   (case CONVERT(nvarchar(10), pye.More_Requested, 101) when '01/01/1900' then '' Else \r\n" +
                        "         Case Convert(nvarchar(10), pye.More_Received, 101) \r\n" +
                        "         When '01/01/1900' then '(Additional Information Requested)' \r\n" +
                        "         Else '' End End) as MoreRequestedText, \r\n" +
                        "   CONVERT(nvarchar(10), pye.Initially_Received, 101) as InitiallyReceivedText, \r\n" +
                        "   CONVERT(nvarchar(10), pye.More_Received, 101) as MoreReceivedText, \r\n" +
                        "   CONVERT(nvarchar(10), pye.Reviewed, 101) as ReviewedText, \r\n" +
                        "   CONVERT(nvarchar(10), pye.More_Received, 101) as MoreReceived, \r\n" +
                        "   IsNull((Select Money_Type_Lib_Text From Money_Type_Lib mtl Where mtl.Money_Type_Lib_ID = at.Money_Type ),'') as MoneyTypeText, \r\n" +
                        "   IsNull(at.Record_Keeper,'') as RecordKeeperText, \r\n" +
                        "   IsNull(at.RK_Account_No,'') as RKAccountNoText, \r\n" +
                        "   IsNull(at.Custodian,'') as CustodianText, \r\n" +
                        "   IsNull(at.C_Account_No, '') as CAccountNoText, \r\n" +
                        "   IsNull(at.Sch_A_Required,0) as SchARequired,\r\n" +
                        "   IsNull(at.NonQualifiedAsset,0) as NonQualifiedAsset,\r\n" +
                        "   IsNull(at.InvestmentPlatform,-1) as InvestmentPlatform,\r\n" +
                        "   ISNULL(mt.Month01, 0) as Month01, ISNULL(mt.Received01, 0) as Received01, \r\n" +
                        "   ISNULL(mt.Month02, 0) as Month02, ISNULL(mt.Received02, 0) as Received02, \r\n" +
                        "   ISNULL(mt.Month03, 0) as Month03, ISNULL(mt.Received03, 0) as Received03, \r\n" +
                        "   ISNULL(mt.Month04, 0) as Month04, ISNULL(mt.Received04, 0) as Received04, \r\n" +
                        "   ISNULL(mt.Month05, 0) as Month05, ISNULL(mt.Received05, 0) as Received05, \r\n" +
                        "   ISNULL(mt.Month06, 0) as Month06, ISNULL(mt.Received06, 0) as Received06, \r\n" +
                        "   ISNULL(mt.Month07, 0) as Month07, ISNULL(mt.Received07, 0) as Received07, \r\n" +
                        "   ISNULL(mt.Month08, 0) as Month08, ISNULL(mt.Received08, 0) as Received08, \r\n" +
                        "   ISNULL(mt.Month09, 0) as Month09, ISNULL(mt.Received09, 0) as Received09, \r\n" +
                        "   ISNULL(mt.Month10, 0) as Month10, ISNULL(mt.Received10, 0) as Received10, \r\n" +
                        "   ISNULL(mt.Month11, 0) as Month11, ISNULL(mt.Received11, 0) as Received11, \r\n" +
                        "   ISNULL(mt.Month12, 0) as Month12, ISNULL(mt.Received12, 0) as Received12, \r\n" +
                        "   IsNull(at.Statement_Delivery,-1) as StatementDelivery, \r\n" +
                        "   pye.PYE_ID, \r\n" + /*Feature #187 Added By Amar to update Followup date on email Followup letter on 19-Feb-2016 */
                        "   pye.TrackingDoc_Type_Add, \r\n" +//Added by nikita for individual sch a tracking
                        "   pye.PYE_Year \r\n" +       //Added By Chandrashekhar For Getting plan year
                        "From PlanYearEnd pye \r\n" +
                        "       Left Join AssetTracking at \r\n" +
                        "           On pye.TrackingDoc_Type_Add = at.AssetTracking_ID \r\n" +
                        "           AND pye.Plans_Index_ID = at.Plans_Index_ID \r\n" +
                        "           AND pye.PYE_Year = at.PYE_Year \r\n" +
                        "       Left Join MonthlyStatement mt \r\n" +
                        "           On mt.PYE_ID = pye.PYE_ID \r\n" +
                        "Where pye.PYE_Year = " + pYE_Year + " \r\n" +
                        "   AND pye.NotRequired = 0 \r\n" +
                        "   AND pye.TrackingDoc_Type_ID <> 10 \r\n" +
                        "   AND (CONVERT(nvarchar(10), pye.Reviewed, 101)) = '01/01/1900' \r\n" +
                        "   AND (\r\n" +
                //      "     (CONVERT(nvarchar(10), pye.Initially_Received, 101) = '01/01/1900') OR \r\n" +//commented by nikita on 17-june-16 
                        "     (CONVERT(nvarchar(10), pye.Reviewed, 101) = '01/01/1900') OR \r\n" +//added by nikita on 17-june-16 
                        "     (CONVERT(nvarchar(10), pye.Initially_Received, 101) <> '01/01/1900' AND \r\n" +
                        "      CONVERT(nvarchar(10), pye.More_Requested, 101) <> '01/01/1900' AND \r\n" +
                        "      CONVERT(nvarchar(10), pye.Reviewed, 101) <> '01/01/1900' AND \r\n" +//added by nikita on 17-june-16 
                        "      CONVERT(nvarchar(10), pye.More_Received, 101) = '01/01/1900')\r\n" +
                        "    ) \r\n" +
                        "   AND (pye.PYE_Year = at.PYE_Year OR pye.TrackingDoc_Type_Add = -1) \r\n" +
                        "   AND (pye.Plans_Index_ID = at.Plans_Index_ID OR pye.TrackingDoc_Type_Add = -1) \r\n" +
                        " And pye.TrackingDoc_Type_ID!=5  AND pye.Plans_Index_ID = " + plans_Index_ID + " \r\n" +
                //// start Added by nikita fro sch a
                        "  union \r\n" +
                        " Select distinct  \r\n" +
                        "  (Select TrackingDoc_Name From TrackingDoc_Type Where TrackingDoc_Type_ID = pye.TrackingDoc_Type_ID) as TrackingDocText, \r\n" +
                        "   pye.TrackingDoc_Type_Add, pye.Plans_Index_ID, \r\n" +
                        "  (case CONVERT(nvarchar(10), pye.More_Requested, 101) when '01/01/1900' then '' Else \r\n" +
                        "         Case Convert(nvarchar(10), pye.More_Received, 101) \r\n" +
                        "         When '01/01/1900' then '(Additional Information Requested)' \r\n" +
                        "         Else '' End End) as MoreRequestedText, \r\n" +
                        "   CONVERT(nvarchar(10), pye.Initially_Received, 101) as InitiallyReceivedText, \r\n" +
                        "   CONVERT(nvarchar(10), pye.More_Received, 101) as MoreReceivedText, \r\n" +
                        "   CONVERT(nvarchar(10), pye.Reviewed, 101) as ReviewedText, \r\n" +
                        "   CONVERT(nvarchar(10), pye.More_Received, 101) as MoreReceived, \r\n" +
                        "   IsNull((Select Money_Type_Lib_Text From Money_Type_Lib mtl Where mtl.Money_Type_Lib_ID = at.Money_Type ),'') as MoneyTypeText, \r\n" +
                        "   IsNull(at.Record_Keeper,'') as RecordKeeperText, \r\n" +
                        "   IsNull(at.RK_Account_No,'') as RKAccountNoText, \r\n" +
                        "   IsNull(at.Custodian,'') as CustodianText, \r\n" +
                        "   IsNull(at.C_Account_No, '') as CAccountNoText, \r\n" +
                        "   IsNull(at.Sch_A_Required,0) as SchARequired,\r\n" +
                        "   IsNull(at.NonQualifiedAsset,0) as NonQualifiedAsset,\r\n" +
                        "   IsNull(at.InvestmentPlatform,-1) as InvestmentPlatform,\r\n" +
                        "   ISNULL(mt.Month01, 0) as Month01, ISNULL(mt.Received01, 0) as Received01, \r\n" +
                        "   ISNULL(mt.Month02, 0) as Month02, ISNULL(mt.Received02, 0) as Received02, \r\n" +
                        "   ISNULL(mt.Month03, 0) as Month03, ISNULL(mt.Received03, 0) as Received03, \r\n" +
                        "   ISNULL(mt.Month04, 0) as Month04, ISNULL(mt.Received04, 0) as Received04, \r\n" +
                        "   ISNULL(mt.Month05, 0) as Month05, ISNULL(mt.Received05, 0) as Received05, \r\n" +
                        "   ISNULL(mt.Month06, 0) as Month06, ISNULL(mt.Received06, 0) as Received06, \r\n" +
                        "   ISNULL(mt.Month07, 0) as Month07, ISNULL(mt.Received07, 0) as Received07, \r\n" +
                        "   ISNULL(mt.Month08, 0) as Month08, ISNULL(mt.Received08, 0) as Received08, \r\n" +
                        "   ISNULL(mt.Month09, 0) as Month09, ISNULL(mt.Received09, 0) as Received09, \r\n" +
                        "   ISNULL(mt.Month10, 0) as Month10, ISNULL(mt.Received10, 0) as Received10, \r\n" +
                        "   ISNULL(mt.Month11, 0) as Month11, ISNULL(mt.Received11, 0) as Received11, \r\n" +
                        "   ISNULL(mt.Month12, 0) as Month12, ISNULL(mt.Received12, 0) as Received12, \r\n" +
                        "   IsNull(at.Statement_Delivery,-1) as StatementDelivery, \r\n" +
                        "   pye.PYE_ID, \r\n" +
                        "   pye.TrackingDoc_Type_Add, \r\n" +
                        "   pye.PYE_Year \r\n" +
                        "From PlanYearEnd pye \r\n" +
                        "       Left Join ScheduleATracking at \r\n" +
                        "           On pye.TrackingDoc_Type_Add = at.ScheduleATracking_ID \r\n" +
                        "           AND pye.Plans_Index_ID = at.Plans_Index_ID \r\n" +
                        "           AND pye.PYE_Year = at.PYE_Year \r\n" +
                        "       Left Join MonthlyStatement mt \r\n" +
                        "           On mt.PYE_ID = pye.PYE_ID \r\n" +
                        "Where pye.PYE_Year = " + pYE_Year + " \r\n" +
                        "   AND pye.NotRequired = 0 \r\n" +
                        "   AND pye.TrackingDoc_Type_ID <> 10 \r\n" +
                        "   AND (CONVERT(nvarchar(10), pye.Reviewed, 101)) = '01/01/1900' \r\n" +
                        "   AND ( \r\n" +
                //      "     (CONVERT(nvarchar(10), pye.Initially_Received, 101) = '01/01/1900') OR \r\n" +//commented by nikita on 17-june-16 
                        "     (CONVERT(nvarchar(10), pye.Reviewed, 101) = '01/01/1900') OR \r\n" +//added by nikita on 17-june-16 
                        "     (CONVERT(nvarchar(10), pye.Initially_Received, 101) <> '01/01/1900' AND \r\n" +
                        "      CONVERT(nvarchar(10), pye.More_Requested, 101) <> '01/01/1900' AND \r\n" +
                        "      CONVERT(nvarchar(10), pye.Reviewed, 101) <> '01/01/1900' AND \r\n" +//added by nikita on 17-june-16 
                        "      CONVERT(nvarchar(10), pye.More_Received, 101) = '01/01/1900')\r\n" +
                        "    ) \r\n" +
                        "   AND (pye.PYE_Year = at.PYE_Year OR pye.TrackingDoc_Type_Add != -1) \r\n" +
                        "   AND (pye.Plans_Index_ID = at.Plans_Index_ID OR pye.TrackingDoc_Type_Add != -1) \r\n" +
                        "  And pye.TrackingDoc_Type_ID!=11 And pye.TrackingDoc_Type_ID!=15  AND pye.Plans_Index_ID = " + plans_Index_ID + " \r\n" +
                //Added By  Chandrashekhar for Market Valuation
                        "  union \r\n" +
                       " Select distinct  \r\n" +
                       "  (Select TrackingDoc_Name From TrackingDoc_Type Where TrackingDoc_Type_ID = pye.TrackingDoc_Type_ID) as TrackingDocText, \r\n" +
                       "   pye.TrackingDoc_Type_Add, pye.Plans_Index_ID, \r\n" +
                       "  (case CONVERT(nvarchar(10), pye.More_Requested, 101) when '01/01/1900' then '' Else \r\n" +
                       "         Case Convert(nvarchar(10), pye.More_Received, 101) \r\n" +
                       "         When '01/01/1900' then '(Additional Information Requested)' \r\n" +
                       "         Else '' End End) as MoreRequestedText, \r\n" +
                       "   CONVERT(nvarchar(10), pye.Initially_Received, 101) as InitiallyReceivedText, \r\n" +
                       "   CONVERT(nvarchar(10), pye.More_Received, 101) as MoreReceivedText, \r\n" +
                       "   CONVERT(nvarchar(10), pye.Reviewed, 101) as ReviewedText, \r\n" +
                       "   CONVERT(nvarchar(10), pye.More_Received, 101) as MoreReceived, \r\n" +
                       "   IsNull((Select Money_Type_Lib_Text From Money_Type_Lib mtl Where mtl.Money_Type_Lib_ID = at.Money_Type ),'') as MoneyTypeText, \r\n" +
                       "   IsNull(at.Record_Keeper,'') as RecordKeeperText, \r\n" +
                       "   IsNull(at.RK_Account_No,'') as RKAccountNoText, \r\n" +
                       "   IsNull(at.Custodian,'') as CustodianText, \r\n" +
                       "   IsNull(at.C_Account_No, '') as CAccountNoText, \r\n" +
                       "   IsNull(at.Sch_A_Required,0) as SchARequired,\r\n" +
                       "   IsNull(at.NonQualifiedAsset,0) as NonQualifiedAsset,\r\n" +
                       "   IsNull(at.InvestmentPlatform,-1) as InvestmentPlatform,\r\n" +
                       "   ISNULL(mt.Month01, 0) as Month01, ISNULL(mt.Received01, 0) as Received01, \r\n" +
                       "   ISNULL(mt.Month02, 0) as Month02, ISNULL(mt.Received02, 0) as Received02, \r\n" +
                       "   ISNULL(mt.Month03, 0) as Month03, ISNULL(mt.Received03, 0) as Received03, \r\n" +
                       "   ISNULL(mt.Month04, 0) as Month04, ISNULL(mt.Received04, 0) as Received04, \r\n" +
                       "   ISNULL(mt.Month05, 0) as Month05, ISNULL(mt.Received05, 0) as Received05, \r\n" +
                       "   ISNULL(mt.Month06, 0) as Month06, ISNULL(mt.Received06, 0) as Received06, \r\n" +
                       "   ISNULL(mt.Month07, 0) as Month07, ISNULL(mt.Received07, 0) as Received07, \r\n" +
                       "   ISNULL(mt.Month08, 0) as Month08, ISNULL(mt.Received08, 0) as Received08, \r\n" +
                       "   ISNULL(mt.Month09, 0) as Month09, ISNULL(mt.Received09, 0) as Received09, \r\n" +
                       "   ISNULL(mt.Month10, 0) as Month10, ISNULL(mt.Received10, 0) as Received10, \r\n" +
                       "   ISNULL(mt.Month11, 0) as Month11, ISNULL(mt.Received11, 0) as Received11, \r\n" +
                       "   ISNULL(mt.Month12, 0) as Month12, ISNULL(mt.Received12, 0) as Received12, \r\n" +
                       "   IsNull(at.Statement_Delivery,-1) as StatementDelivery, \r\n" +
                       "   pye.PYE_ID, \r\n" +
                       "   pye.TrackingDoc_Type_Add, \r\n" +
                       "   pye.PYE_Year \r\n" +
                       "From PlanYearEnd pye \r\n" +
                       "       Left Join MarketValuationTracking at \r\n" +
                       "           On pye.TrackingDoc_Type_Add = at.MarketValuation_ID \r\n" +
                       "           AND pye.Plans_Index_ID = at.Plans_Index_ID \r\n" +
                       "           AND pye.PYE_Year = at.PYE_Year \r\n" +
                       "       Left Join MonthlyStatement mt \r\n" +
                       "           On mt.PYE_ID = pye.PYE_ID \r\n" +
                       "Where pye.PYE_Year = " + pYE_Year + " \r\n" +
                       "   AND pye.NotRequired = 0 \r\n" +
                       "   AND pye.TrackingDoc_Type_ID <> 10 \r\n" +
                       "   AND (CONVERT(nvarchar(10), pye.Reviewed, 101)) = '01/01/1900' \r\n" +
                       "   AND ( \r\n" +
                //     "     (CONVERT(nvarchar(10), pye.Initially_Received, 101) = '01/01/1900') OR \r\n" +//commented by nikita on 17-june-16 
                       "     (CONVERT(nvarchar(10), pye.Reviewed, 101) = '01/01/1900') OR \r\n" +//added by nikita on 17-june-16 
                       "     (CONVERT(nvarchar(10), pye.Initially_Received, 101) <> '01/01/1900' AND \r\n" +
                       "      CONVERT(nvarchar(10), pye.More_Requested, 101) <> '01/01/1900' AND \r\n" +
                       "      CONVERT(nvarchar(10), pye.Reviewed, 101) <> '01/01/1900' AND \r\n" +//added by nikita on 17-june-16 
                       "      CONVERT(nvarchar(10), pye.More_Received, 101) = '01/01/1900')\r\n" +
                       "    ) \r\n" +
                       "   AND (pye.PYE_Year = at.PYE_Year OR pye.TrackingDoc_Type_Add != -1) \r\n" +
                       "   AND (pye.Plans_Index_ID = at.Plans_Index_ID OR pye.TrackingDoc_Type_Add != -1) \r\n" +
                       "  And pye.TrackingDoc_Type_ID!=11  And pye.TrackingDoc_Type_ID!=5 AND pye.Plans_Index_ID = " + plans_Index_ID + " \r\n";


            // end Added by nikita fro sch a
            // commited on 22Mar16
            //  if (plans_Index_ID > -1)
            //    sqlCmd += " AND pye.Plans_Index_ID = " + plans_Index_ID;
            dt = ExecuteSqlCommand(sqlCmd, con);
            List<FollowUpLetterModel> data = ConvertDataTableToList<FollowUpLetterModel>(dt);
            //  List<FollowUpLetterModel> data = entity.ExecuteStoreQuery<FollowUpLetterModel>(sqlCmd).ToList();
            //Start Edited by nikita
            string cmd1 = "select top 1 py2y.Type5500 from Plans_YearToYear py2y join Plans p on py2y.Plans_Index_ID=p.Plans_Index_ID where py2y.Plans_Index_ID=" + plans_Index_ID + " and py2y.PYE_Year=" + pYE_Year + " ";
            //List<int> result = entity.ExecuteStoreQuery<int>(cmd1).ToList();
            dt = ExecuteSqlCommand(cmd1, con);
            List<FollowupsValue> result = ConvertDataTableToList<FollowupsValue>(dt);

            List<FollowUpLetterModel> dataToRemove = new List<FollowUpLetterModel>();

            string cmmd = " select cm.TaxedAs from Plans p join Plans_YearToYear py2y on p.Plans_Index_ID=py2y.Plans_Index_ID join Client_Master cm on p.Client_Id=cm.Client_Id where cm.TaxedAs!=-1 and p.Plans_Index_ID=" + plans_Index_ID + " and py2y.PYE_Year=" + pYE_Year + "";
            List<FollowupsValue> TaskedAsValue = null;
            if (cmmd != null)
            {
                //  List<int> taxedAsID = entity.ExecuteStoreQuery<int>(cmmd).ToList();
                dt = ExecuteSqlCommand(cmmd, con);
                List<FollowupsValue> taxedAsID = ConvertDataTableToList<FollowupsValue>(dt);
                if (taxedAsID.Count != 0)
                {
                    string cmd3 = "select TaxedAs_Text from TaxedAs where TaxedAs_ID=" + taxedAsID[0].TaxedAs + "";
                    // TaskedAsValue = entity.ExecuteStoreQuery<string>(cmd3).ToList();
                    dt = ExecuteSqlCommand(cmd3, con);
                    TaskedAsValue = ConvertDataTableToList<FollowupsValue>(dt);
                }
            }

            //Remove fidility bond and census
            if (pYE_Year >= Convert.ToInt32(ConfigurationManager.AppSettings["AFFECT_YEAR"]))
            {
                foreach (var followupdata in data)
                {
                    if (followupdata.TrackingDocText == "Fidelity Bond")
                    {
                        if (result.Count != 0 && result[0].Type5500 != -1 && result[0].Type5500 == 3)
                        {
                            dataToRemove.Add(followupdata);
                        }
                    }
                    if (followupdata.TrackingDocText == "Census" && TaskedAsValue[0].TaxedAs_Text != null && !(TaskedAsValue[0].TaxedAs_Text == "Corporation" || TaskedAsValue[0].TaxedAs_Text == "Sub-Chapter S Corporation") && pYE_Year < 2016)
                    {
                        if (result.Count != 0 && result[0].Type5500 != -1 && result[0].Type5500 == 3)
                        {
                            dataToRemove.Add(followupdata);
                        }
                    }
                }
            }
            if (dataToRemove.Count() > 0)
            {
                foreach (var x in dataToRemove)
                {
                    data.Remove(x);
                }
            }

            //check whether accets Sachdule A is checked

            List<PlanAssetModel> assets = gatherPlanAssets(plans_Index_ID, pYE_Year, " ", "", 0);
            int schADataCount = 0;


            foreach (var schAData in assets)
            {
                if (schAData.Sch_A_Required == true)
                {
                    schADataCount++;
                }
            }
            //Added by chandrashekhar for marketvaluation
            List<MarketValuationModel> mvasset = GatherMarketValuationTracking(plans_Index_ID, pYE_Year, " ", "", 0);
            int mvDataCount = 0;
            foreach (var mvData in mvasset)
            {
                if (mvData.NonQualifiedAsset == true || mvData.Statement_Delivery == 6)
                {
                    mvDataCount++;
                }
            }
            if (pYE_Year >= Convert.ToInt32(ConfigurationManager.AppSettings["AFFECT_YEAR"]) && schADataCount == 0)
            {
                foreach (var followupdata in data)
                {
                    if (followupdata.TrackingDocText == "Schedule A")
                    {
                        data.Remove(followupdata);
                        break;
                    }
                }
            }
            //Added by Chandrashekhar
            if (pYE_Year >= Convert.ToInt32(ConfigurationManager.AppSettings["AFFECT_YEAR"]) && mvDataCount == 0)
            {
                foreach (var followupdata in data)
                {
                    if (followupdata.TrackingDocText == "Market Valuation")
                    {
                        data.Remove(followupdata);
                        break;
                    }
                }
            }
            //check tasked as value for active Plan year -remove Sch K-1 and Sch C
            string cmd2 = " select cm.TaxedAs from Plans p join Plans_YearToYear py2y on p.Plans_Index_ID=py2y.Plans_Index_ID join Client_Master cm on p.Client_Id=cm.Client_Id where cm.TaxedAs!=-1 and py2y.ActivePlanYear=1 and p.Plans_Index_ID=" + plans_Index_ID + " and py2y.PYE_Year=" + pYE_Year + "";
            if (cmd2 != null)
            {
                // List<int> taxedAs = entity.ExecuteStoreQuery<int>(cmd2).ToList();
                dt = ExecuteSqlCommand(cmd2, con);
                List<FollowupsValue> taxedAs = ConvertDataTableToList<FollowupsValue>(dt);

                if (taxedAs.Count != 0)
                {
                    //string cmd3 = "select TaxedAs_Text from TaxedAs where TaxedAs_ID=" + taxedAs[0] + "";
                    //List<string> TaskedAsText = entity.ExecuteStoreQuery<string>(cmd3).ToList();
                    string cmd3 = "select TaxedAs_ID from TaxedAs where TaxedAs_ID=" + taxedAs[0].TaxedAs + "";
                    // List<int> TaskedAsText = entity.ExecuteStoreQuery<int>(cmd3).ToList();
                    dt = ExecuteSqlCommand(cmd3, con);
                    List<FollowupsValue> TaskedAsText = ConvertDataTableToList<FollowupsValue>(dt);
                    List<FollowUpLetterModel> dataToRemoveTaxedAs = new List<FollowUpLetterModel>();
                    foreach (var followupdata in data)
                    {
                        //remove Sch K-1 and Sch C
                        if ((TaskedAsText[0].TaxedAs_ID != 2 && followupdata.TrackingDocText == "Schedule K-1") || (TaskedAsText[0].TaxedAs_ID != 3 && followupdata.TrackingDocText == "Schedule C"))
                        {
                            dataToRemoveTaxedAs.Add(followupdata);
                        }
                    }

                    if (dataToRemoveTaxedAs.Count() > 0)
                    {
                        foreach (var x in dataToRemoveTaxedAs)
                        {
                            data.Remove(x);
                        }
                    }
                }
            }

            //remove COD if Plantype is not psk-For active year
            string cmdPlanType = " select p.Plan_Type_Code from Plans p join Plans_YearToYear py2y on p.Plans_Index_ID=py2y.Plans_Index_ID where  py2y.ActivePlanYear=1 and p.Plans_Index_ID=" + plans_Index_ID + " and py2y.PYE_Year=" + pYE_Year + "";
            if (pYE_Year >= Convert.ToInt32(ConfigurationManager.AppSettings["AFFECT_YEAR"]))
            {
                // List<int> PlanType = entity.ExecuteStoreQuery<int>(cmdPlanType).ToList();
                dt = ExecuteSqlCommand(cmdPlanType, con);
                List<FollowupsValue> PlanType = ConvertDataTableToList<FollowupsValue>(dt);
                if (PlanType.Count != 0 && PlanType[0].Plan_Type_Code != 14 && PlanType[0].Plan_Type_Code != 2) //added by chandrashekhar for 403(b) and PSK Plan
                {
                    foreach (var followupdata in data)
                    {
                        if (followupdata.TrackingDocText == "Confirmation of 401(k) Deposits")
                        {
                            data.Remove(followupdata);
                            break;
                        }
                    }
                }
            }
            //End Edited by nikita
            return data;
        }

        /// <summary>
        /// gather Plan Assets
        /// </summary>
        public static List<PlanAssetModel> gatherPlanAssets(int Plan_Index_Id, int year, string jtSorting, string TokenID, int ConnectionIndex)
        {
            try
            {
                SqlConnection con = new SqlConnection(dbCon);
                string cmd = "SELECT at.AssetTracking_ID, at.Plans_Index_ID, at.Record_Keeper, at.RK_Account_No,at.Account_Type, (Select AccountType_Lib_Text From AccountType_Lib Where AccountType_Lib_ID=Account_Type) as Account_Type_Text , " +
                            " (Select Money_Type_Lib.Money_Type_Lib_Text From Money_Type_Lib Where Money_Type_Lib.Money_Type_Lib_ID = at.Money_Type) as MoneyType,   " +
                            " (Case pye.Notes_ID When -1 Then '../../Content/Images/blank.GIF' Else '../../Content/Images/note_edit_btn.png' End) as HasNotesGlyph,  " +
                             " pye.PYE_ID, pye.Notes_ID,at.Sch_A_Required, Case When at.InvestmentPlatform != -1 Then IsNull((Select InvestmentPlatform_Text From InvestmentPlatforms_Lib  " +
                             " Where InvestmentPlatform_ID = at.InvestmentPlatform), '[Not Set]') Else '[Not Set]' End as Statement_From_Text,  " +
                            " (Case Convert(nvarchar(10), LastStatementDate, 101) when '01/01/1900' then '' else Convert(nvarchar(10), LastStatementDate, 101) end) as Last_Statement_Date_Text  " +
                            " FROM AssetTracking at Right Join PlanYearEnd pye On at.AssetTracking_ID = pye.TrackingDoc_Type_Add " +
                            " WHERE at.Plans_Index_ID=" + Plan_Index_Id + " AND at.PYE_Year=" + year + " AND pye.TrackingDoc_Type_ID = 11 AND pye.Plans_Index_ID=" + Plan_Index_Id + " AND pye.PYE_Cycle = 1 ";
                if (jtSorting.Trim() != "")
                {
                    cmd += "Order By " + jtSorting;
                }
                else
                {
                    cmd += "Order By Asset_Index";
                }

                //List<PlanAssetModel> data = entity.ExecuteStoreQuery<PlanAssetModel>(cmd).ToList();
                dt = ExecuteSqlCommand(cmd, con);
                List<PlanAssetModel> data = ConvertDataTableToList<PlanAssetModel>(dt);

                //for (int i = 0; i < data.Count; i++)
                //{

                //    data[i].statusColor = getAssetColor(data[i].AssetTracking_ID, data[i].PYE_ID);

                //}

                return data;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Gather Market ValuationTracking
        /// </summary>
        public static List<MarketValuationModel> GatherMarketValuationTracking(int Plan_Index_Id, int year, string jtSorting, string TokenID, int ConnectionIndex)
        {
            try
            {
                SqlConnection con = new SqlConnection(dbCon);
                string sqlCmd = "SELECT *, \r\n" +
                               "    (case at.AssetConfirmedByType \r\n" +
                               "        WHEN 0 THEN \r\n" +
                               "            (SELECT First_Name + ' ' + Last_Name FROM UserTable WHERE SysUserID = at.AssetConfirmedByWho) \r\n" +
                               "        WHEN 1 THEN \r\n" +
                               "            (SELECT First_Name + ' ' + Last_Name FROM Contacts WHERE Contact_ID = at.AssetConfirmedByWho) \r\n" +
                               "        WHEN 2 THEN \r\n" +
                               "            (SELECT First_Name + ' ' + Last_Name FROM Advisor WHERE Advisor_ID = at.AssetConfirmedByWho) \r\n" +
                               "        ELSE '' END \r\n" +
                               "    ) as ConfirmedByWho, \r\n" +
                               "    IsNull((Select Client_ID  \r\n" +
                               "        From Plans  \r\n" +
                               "        Where Plans.Plans_Index_ID=at.Plans_Index_ID \r\n" +
                               "    ), -1) as Client_ID,  \r\n" +
                               "    IsNull((Select AccountType_Lib_Text  \r\n" +
                               "        From AccountType_Lib  \r\n" +
                               "        Where AccountType_Lib_ID=Account_Type \r\n" +
                               "    ), '[Not Set]') as Account_Type_Text,  \r\n" +
                               "    IsNull((Select StatementDelivery_Type_Text  \r\n" +
                               "        From StatementDelivery_Type  \r\n" +
                               "        Where StatementDelivery_Type_ID = Statement_Delivery \r\n" +
                               "    ), '[Not Set]') as Statement_Delivery_Text,  \r\n" +
                               "    IsNull((Case When (at.Kind = 'Advisor' or at.Kind Is Null)  \r\n" +
                               "        Then (Select First_Name + ' ' + Last_Name    \r\n" +
                               "  		    From Advisor    \r\n" +
                               "  	        Where Advisor_ID = at.Advisor)  \r\n" +
                               "        When at.Kind = 'Contact' \r\n" +
                               "            Then (Select First_Name + ' ' + Last_Name    \r\n" +
                               "                From Contacts    \r\n" +
                               "                Where Contact_ID = at.Advisor)   \r\n" +
                               "        End), \r\n" +
                               "   '(None)') as Advisor_Name, \r\n" +

                               "    (Case Notes  \r\n" +
                               "        When 1 Then 'X'  \r\n" +
                               "        Else '' End \r\n" +
                               "    ) as Notes_Text,  \r\n" +
                               "    (Case More  \r\n" +
                               "        When 1 Then 'X'  \r\n" +
                               "        Else '' End \r\n" +
                               "    ) as More_Text,  \r\n" +
                               "    (Case Sch_A_Required  \r\n" +
                               "        When 1 Then 'X'  \r\n" +
                               "        Else '' End \r\n" +
                               "    ) as Sch_A_Text,  \r\n" +
                               "    IsNull((Select Money_Type_Lib.Money_Type_Lib_Text  \r\n" +
                               "        From Money_Type_Lib  \r\n" +
                               "        Where Money_Type_Lib.Money_Type_Lib_ID = at.Money_Type \r\n" +
                               "    ), '[Not Set]') as MoneyType,  \r\n" +
                               "    (Case Same_As  \r\n" +
                               "        When -1 Then 'None'  \r\n" +
                               "        Else Cast(Same_As as nvarchar(3)) End \r\n" +
                               "    ) as Same_as_Text,  \r\n" +
                               "    (Case QBSAction  \r\n" +
                               "        When 1 Then 'Upload'  \r\n" +
                               "        When 2 Then 'Send'  \r\n" +
                               "        When 3 Then 'Upload And Send'  \r\n" +
                               "        When 4 Then 'N/A'  \r\n" +
                               "        Else '[Not Set]' End \r\n" +
                               "    ) as QBS_Text,  \r\n" +
                               "    (Case Notes_ID  \r\n" +
                               "          When -1 Then '../../Content/Images/blank.GIF'  \r\n" +
                               "          Else '../../Content/Images/note_edit_btn.png' End \r\n" +

                               "    ) as HasNotesGlyph,  \r\n" +
                               "    IsNull((Select pye.Actual_Document  \r\n" +
                               "        From PlanYearEnd pye  \r\n" +
                               "        Where pye.Plans_Index_ID = " + Plan_Index_Id + " \r\n" +
                               "            And pye.PYE_Year = " + year + "  \r\n" +
                               "            And pye.TrackingDoc_Type_ID = 15   \r\n" +
                               "            And pye.TrackingDoc_Type_Add = at.MarketValuation_ID \r\n" +
                               "    ), -1) as Actual_Document,  \r\n" +

                               "    Case When at.InvestmentPlatform != -1 Then \r\n" +
                               "            IsNull((Select InvestmentPlatform_Text From InvestmentPlatforms_Lib  \r\n" +
                               "            Where InvestmentPlatform_ID = at.InvestmentPlatform), \r\n" +
                               "            '[Not Set]') \r\n" +
                               "         Else '[Not Set]' \r\n" +
                               "    End as Statement_From_Text,  \r\n" +

                               "    (Case Convert(nvarchar(10), LastStatementDate, 101) when '01/01/1900' then '' \r\n" +
                               "        Else Convert(nvarchar(10), LastStatementDate, 101) End) \r\n" +
                               "     as Last_Statement_Date_Text  \r\n" +

                               "FROM MarketValuationTracking at  \r\n" +
                               "    WHERE Plans_Index_ID=" + Plan_Index_Id + " AND PYE_Year=" + year + " \r\n";
                if (jtSorting.Trim() != "")
                {
                    sqlCmd += "Order By " + jtSorting;
                }
                else
                {
                    sqlCmd += "Order By Asset_Index";
                }
                //List<MarketValuationModel> data = entity.ExecuteStoreQuery<MarketValuationModel>(sqlCmd).ToList();
                dt = ExecuteSqlCommand(sqlCmd, con);
                List<MarketValuationModel> data = ConvertDataTableToList<MarketValuationModel>(dt);
                return data;
            }
            catch (Exception)
            {
                throw;
            }
        }



        /// <summary>
        /// The hide acct num.
        /// </summary>
        /// <param name="accNo">
        /// The acc no.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string HideAcctNum(string accNo)
        {
            try
            {
                var res = string.Empty;
                if (accNo.Length >= 4)
                {
                    for (var i = 0; i < accNo.Length - 4; i++)
                    {
                        if (accNo.Substring(i, 1) == "-")
                        {
                            res += "-";
                        }
                        else
                        {
                            res += "X";
                        }
                    }

                    res += accNo.Substring(accNo.Length - 4, 4);
                    return res;
                }
                else
                {
                    for (var i = 0; i < accNo.Length; i++)
                    {
                        if (accNo.Substring(i, 1) == "-")
                        {
                            res += "-";
                        }
                        else
                        {
                            res += "X";
                        }
                    }
                    return res;
                }
            }
            catch (System.Exception)
            {
                throw;
            }
        }


        /// <summary>
        /// Generate followup latter
        /// </summary>
        /// <param name="pYEYear"></param>

        /// <param name="obj"></param>
        /// <returns></returns>
        public static CommModule_AllPredefinedKey getFollowUpInformation(int Level, int clientId, SqlConnection con, CommModule_AllPredefinedKey obj, string bodyContent, int Contact_Id, string contactType, bool isblank)
        {
            string isfollowups = string.Empty;
            List<Client_info> clientdata = new List<Client_info>();
            string dateOfRecord = DateTime.UtcNow.ToString("MMMM dd, yyyy");
            //  clientdata = new List<Client_info>();
            //bool isblank = bodyContent.Contains("[FollowupsCheckList1]");
            string Followup_Signature = string.Empty;
            cmdStr = " select py.PYE_Year,p.Plans_Index_ID,p.Plan_Number,p.Plan_Name,p.PlanYE,cl.Client_Name,cl.Client_ID from Plans p join Plans_YearToYear py on p.Plans_Index_ID=py.Plans_Index_ID  join Client_Master cl on cl.Client_ID= p.Client_ID where p.Client_ID=" + clientId + " and py.ActivePlanYear=" + 1 + " and py.IsHoldFollowUp=" + 0 + " ";
            dt = ExecuteSqlCommand(cmdStr, con);
            int pYEYear=0;
            clientdata = ConvertDataTableToList<Client_info>(dt);
            if (clientdata.Count > 0)
            {
                pYEYear = clientdata[0].PYE_Year;
            }
            //int pYEYear = DateTime.UtcNow.Year;
            //var month = clientdata[0].PlanYE.Split('/');
            //int yearMonth = Convert.ToInt16(month[0]);
            //int yearDay = Convert.ToInt16(month[1]);

            //string userInitials = ((Session["userIn"].ToString()).Length > 0) ? Session["userIn"].ToString() : null;
            string userInitials = null;
            string htmlstr = string.Empty;
            string message = string.Empty;
            string error = "There is no data available for these clients:";
            string Messgae = "";
            Dictionary<int, int> dictDocs = new Dictionary<int, int>();
            string htmlDoc = null;
            List<int> filterClientIdList = new List<int>();
            List<string> clientnameList = new List<string>();
            List<LetterCCInfoModel> lettercCInfo = new List<LetterCCInfoModel>();
            List<List<LetterCCInfoModel>> lettercCInfo1 = new List<List<LetterCCInfoModel>>();
            List<LetterInfoModel> letterInfo = new List<LetterInfoModel>();
            List<List<FollowUpLetterModel>> followUpInfo = new List<List<FollowUpLetterModel>>();
            List<LetterInfoModel> contactAdvisorData = getContactAdvisorInfo(clientId, Contact_Id, contactType);

            obj = getContactAdvisorData(bodyContent,contactAdvisorData,obj);

            int i = 0;
            int fuCount = 0;
            if (clientdata.Count > 0)
            {
                foreach (Client_info cid in clientdata)
                {
                    //Nikita
                    List<LetterCCInfoModel> letterCcData = getGenericLetterCCInfo(cid.Client_ID, cid.Plan_Number, string.Empty, 0);
                    List<LetterInfoModel> letterData = getGenericLetterInfo(cid.Client_ID, cid.Plan_Number, string.Empty, 0, Contact_Id);
                    List<FollowUpLetterModel> followUpData = getFollowUpLetterInfo(cid.Plans_Index_ID, cid.PYE_Year, string.Empty, 0);
                    obj.followUpData=followUpData;
                    if (letterData.Count == 0 || followUpData.Count == 0)
                    {
                        // var clientName = "";
                        clientnameList.Add(cid.Client_Name);
                    }
                    if (letterData.Count != 0 && followUpData.Count != 0 && cid.PYE_Year > 1920)
                    {
                        if (letterCcData.Count > 0)
                        {
                            for (int x = 0; x < letterCcData.Count; x++)
                            {
                                lettercCInfo.Add(letterCcData[x]);
                            }
                        }
                        else
                        {
                            lettercCInfo.Add(new LetterCCInfoModel() { Client_ID = cid.Client_ID, Credentials = "", Enclosures = "", First_Name = "", Last_Name = "", MI = "", Via = "" });
                        }

                        foreach (var data in letterData)
                        {
                            //if (data.PrimaryContact == true)
                            //{
                            letterInfo.Add(data);
                            //}
                        }

                        if (letterInfo.Count == 0)
                        {
                            i++;
                        }

                        followUpInfo.Add(new List<FollowUpLetterModel>());

                        if (followUpData.Count > 0)
                        {
                            for (int j = 0; j < followUpData.Count; j++)
                            {

                                //followUpInfo.Insert(i, new List<FollowUpLetterModel> { followUpData[j] });
                                followUpData[j].Client_ID = cid.Client_ID;
                                followUpInfo[fuCount].Add(followUpData[j]);
                            }
                        }
                        fuCount++;
                        filterClientIdList.Add(cid.Client_ID);
                    }
                    i++;
                }
            }

            List<List<LetterCCInfoModel>> filterLettercCInfo = new List<List<LetterCCInfoModel>>();
            List<List<LetterInfoModel>> filterLetterInfo = new List<List<LetterInfoModel>>();
            List<List<FollowUpLetterModel>> filterFollowUpInfo = new List<List<FollowUpLetterModel>>();

            /* Sort the lists according to Client_ID*/
           
                List<LetterCCInfoModel> sortedLetterCcInfo = lettercCInfo.OrderBy(p => p.Client_ID).ToList();
                List<List<FollowUpLetterModel>> sortedFollowInfo = followUpInfo.OrderBy(p => p[0].Client_ID).ToList();
                List<LetterInfoModel> sortedLetterInfo = letterInfo.OrderBy(p => p.Client_ID).ToList();
                filterClientIdList.Sort();


                /* To get CC Info*/
                int fLCCount = 0;
                for (int x = 0; x < sortedLetterCcInfo.Count; x++)
                {
                    filterLettercCInfo.Add(new List<LetterCCInfoModel>());
                    filterLettercCInfo[fLCCount].Add(sortedLetterCcInfo[x]);
                    for (int y = x + 1; y < sortedLetterCcInfo.Count; y++)
                    {
                        if (sortedLetterCcInfo[x].Client_ID == sortedLetterCcInfo[y].Client_ID)
                        {
                            //filterLettercCInfo.Add(new List<LetterCCInfoModel>());
                            filterLettercCInfo[fLCCount].Add(sortedLetterCcInfo[y]);
                            x++;
                        }
                    }
                    fLCCount++;
                }
                /* End CC Info */

                int ccCount = 0;


                //if (filterClientIdList.Count > 0 && clientnameList.Count == 0)
                if (filterClientIdList.Count > 0)
                {
                    //upload Attachments first one time only
                    //  List<BoycePensions.Models.Document> attachmentUploadedblobList = attachmntSrv.UploadCommAttachmentToBlob(attachmentTemp);

                    for (int x = 0; x < filterClientIdList.Count; x++)
                    {
                        int liCount = 0;

                        filterLetterInfo.Add(new List<LetterInfoModel>());
                        filterLetterInfo[liCount].Add(sortedLetterInfo[x]);
                        filterFollowUpInfo.Add(new List<FollowUpLetterModel>());
                        filterFollowUpInfo.Add(sortedFollowInfo[x]);
                        for (int y = x + 1; y < filterClientIdList.Count; y++)
                        {
                            if (filterClientIdList[x] == filterClientIdList[y])
                            {
                                filterLetterInfo.Add(new List<LetterInfoModel>());
                                filterLetterInfo[liCount + 1].Add(sortedLetterInfo[y]);

                                filterFollowUpInfo.Add(sortedFollowInfo[y]);
                                x++;
                                liCount++;
                            }
                        }
                        string Header = string.Empty;
                        if (filterLettercCInfo.Count != 0 && filterLetterInfo.Count != 0 && filterLettercCInfo.Count != 0)
                        {
                            List<DateTime> dtlst = new List<DateTime>();
                            for (int a = 0; a < letterInfo.Count; a++)
                            {
                                var pyedt = letterInfo[a].PlanYE.Split('/');
                                int yearMonth1 = Convert.ToInt16(pyedt[0]);
                                int yearDay1 = Convert.ToInt16(pyedt[1]);

                                DateTime newdt = DateTime.UtcNow;
                                newdt = new DateTime(newdt.Year, yearMonth1, yearDay1);
                                dtlst.Add(newdt);
                            }

                            DateTime mindt = dtlst.Min(s => s);

                            //var month = letterInfo[0][0].PlanYE.Split('/');
                            //int yearMonth = Convert.ToInt16(month[0]);
                            //int yearDay = Convert.ToInt16(month[1]);
                            int yearMonth = mindt.Month;
                            int yearDay = mindt.Day;

                            string cmd3 = "select * from PortalCommModule_FollowupData";

                            dt = ExecuteSqlCommand(cmd3, con);
                            List<PortalCommModule_FollowupData> followupdays = ConvertDataTableToList<PortalCommModule_FollowupData>(dt);

                            var pyeDate = new DateTime(pYEYear, yearMonth, yearDay);


                            //DateTime rushStart = pyeDate.AddMonths(7);
                            //DateTime rushStart = pyeDate.AddMonths(7);
                            //rushStart = new DateTime(rushStart.Year, rushStart.Month, 15);
                            DateTime rushStart = pyeDate.AddDays(followupdays[1].Value);

                            //DateTime rushEnd = pyeDate.AddMonths(8);
                            DateTime rushEnd = pyeDate.AddDays(followupdays[2].Value);
                           // rushEnd = new DateTime(rushEnd.Year, rushEnd.Month, 15);

                            DateTime extendedDate = pyeDate.AddMonths(9);
                            extendedDate = new DateTime(extendedDate.Year, extendedDate.Month, 15);

                            DateTime unExtDate = pyeDate.AddMonths(7);

                            //For Extended Contribution Date
                            DateTime contributionDate = pyeDate.AddMonths(9);
                            contributionDate = new DateTime(contributionDate.Year, contributionDate.Month, 15);
                            //For Funding Deadline
                            //DateTime fundingDeadline = pyeDate.AddMonths(3);
                            //fundingDeadline = new DateTime(fundingDeadline.Year, fundingDeadline.Month, 15);

                            DateTime minimumFundingDeadLine = pyeDate.AddMonths(9);
                            minimumFundingDeadLine = new DateTime(contributionDate.Year, contributionDate.Month, 15);

                            DateTime Ext5500_RequiredDate = pyeDate.AddDays(followupdays[0].Value);
                            DateTime secondrushStart = pyeDate.AddDays(followupdays[3].Value);
                            DateTime secondrushEnd = pyeDate.AddDays(followupdays[4].Value);

                            //  string dateOfRecord = DateTime.UtcNow.ToString("MMMM dd, yyyy");
                            //  obj.Followups = isfollowups;
                            // obj.UserName = contactListArr[1];
                            obj.Date = dateOfRecord;
                            obj.FirstName = filterLetterInfo[0][0].First_Name == null ? "" : filterLetterInfo[0][0].First_Name;
                            obj.LastName = filterLetterInfo[0][0].Last_Name == null ? "" : filterLetterInfo[0][0].Last_Name;
                            obj.MI = filterLetterInfo[0][0].MI == null ? "" : filterLetterInfo[0][0].MI;
                            obj.Address1 = letterInfo[0].Address1 == null ? "" :letterInfo[0].Address1;
                            obj.Address2 = filterLetterInfo[0][0].Address2 == null ? "" :filterLetterInfo[0][0].Address2;
                            obj.City = filterLetterInfo[0][0].City == null ? "" :filterLetterInfo[0][0].City;
                            obj.State_abv = filterLetterInfo[0][0].State_Abrev == null ? "" :filterLetterInfo[0][0].State_Abrev;
                            obj.zipcode = filterLetterInfo[0][0].Zip_Code == null ? "" :filterLetterInfo[0][0].Zip_Code;
                            obj.PhysicalAddress1 = letterInfo[0].PhysicalAddress1 == null ? "" :letterInfo[0].PhysicalAddress1;
                            obj.PhysicalAddress2 = filterLetterInfo[0][0].PhysicalAddress2 == null ? "" :filterLetterInfo[0][0].PhysicalAddress2;
                            obj.PhysicalCity = filterLetterInfo[0][0].City == null ? "" :filterLetterInfo[0][0].City;
                            obj.PhysicalState_abv = filterLetterInfo[0][0].PhysicalState_abv == null ? "" :filterLetterInfo[0][0].PhysicalState_abv;
                            obj.PhysicalZipCode = filterLetterInfo[0][0].PhysicalZipCode == null ? "" :filterLetterInfo[0][0].PhysicalZipCode;
                            obj.Greetingtext = filterLetterInfo[0][0].Greeting_Text == null ? "" :filterLetterInfo[0][0].Greeting_Text;
                            obj.planyearEndDate = pyeDate.ToString("MMMM dd, yyyy") == null ? "" :pyeDate.ToString("MMMM dd, yyyy");
                            obj.StartDate = rushStart.ToString("MMMM dd, yyyy") == null ? "" :rushStart.ToString("MMMM dd, yyyy");
                            obj.EndDate = rushEnd.ToString("MMMM dd, yyyy") == null ? "" :rushEnd.ToString("MMMM dd, yyyy");
                            obj.secondndRushChgStartDate = secondrushStart.ToString("MMMM dd, yyyy") == null ? "" :secondrushStart.ToString("MMMM dd, yyyy");
                            obj.secondndRushChgEndDate = secondrushEnd.ToString("MMMM dd, yyyy") == null ? "" :secondrushEnd.ToString("MMMM dd, yyyy");

                            obj.extendeddate = extendedDate.ToString("MMMM dd, yyyy") == null ? "" :extendedDate.ToString("MMMM dd, yyyy");
                            obj.PAame = filterLetterInfo[0][0].CSRFull == null ? "" :filterLetterInfo[0][0].CSRFull;
                            obj.Title = filterLetterInfo[0][0].CSRTitle == null ? "" :filterLetterInfo[0][0].CSRTitle;
                            obj.CsrInitial = filterLetterInfo[0][0].CSRInitials == null ? "" :filterLetterInfo[0][0].CSRInitials;
                            obj.UnExtDate = unExtDate.ToString("MMMM dd, yyyy") == null ? "" :unExtDate.ToString("MMMM dd, yyyy");
                            obj.ContriButionDateFundingDeadline = contributionDate.ToString("MMMM dd, yyyy") == null ? "" :contributionDate.ToString("MMMM dd, yyyy");
                            obj.MinmumFundingDeadLine = minimumFundingDeadLine.ToString("MMMM dd, yyyy") == null ? "" :minimumFundingDeadLine.ToString("MMMM dd, yyyy");
                            obj.Ext5500_RequiredDate = Ext5500_RequiredDate.ToString("MMMM dd, yyyy") == null ? "" :Ext5500_RequiredDate.ToString("MMMM dd, yyyy");

                            htmlstr = GenrateFollowupLetter1(filterLetterInfo, filterLettercCInfo[ccCount], filterFollowUpInfo, pYEYear, userInitials, "", htmlDoc, "", 1, "", isblank, obj);
                            isfollowups += htmlstr;
                            obj.Followups = isfollowups;

                            Header = "<p style='margin:0;padding:0;'>" + obj.FirstName + " &nbsp; " + obj.MI + "&nbsp; " + obj.LastName + "</p>";
                            Header = Header + "<p style='margin:0;padding:0;'>" + obj.Address1 + " &nbsp; " + obj.Address2 + "</p>";
                            Header = Header + "<p style='margin:0;padding:0;'>" + obj.City + " &nbsp; " + obj.State_abv + "&nbsp; " + obj.zipcode + "</p>";

                            obj.Header = Header;
                        }
                    }
                }
            
            return obj;
        }

        /// <summary>
        ///Set PlanYearEnd after sending the email
        /// </summary>
        static void SetPlanYearEnd(List<string> clientlist, SqlConnection con, int followupType, int? CreatedbyWho)
        {
            DateTime currentDate = DateTime.UtcNow.Date;

            foreach (string clientId in clientlist)
            {

                List<Client_info> clientdata = new List<Client_info>();
                int DeliveryType = 2;
                cmdStr = " select py.PYE_Year,p.Plans_Index_ID,p.Plan_Number,p.Plan_Name,p.PlanYE,cl.Client_Name,cl.Client_ID from Plans p join Plans_YearToYear py on p.Plans_Index_ID=py.Plans_Index_ID  join Client_Master cl on cl.Client_ID= p.Client_ID where p.Client_ID=" + clientId + " and py.ActivePlanYear=" + 1 + " and py.IsHoldFollowUp=" + 0 + " ";
                dt = ExecuteSqlCommand(cmdStr, con);
                clientdata = ConvertDataTableToList<Client_info>(dt);
                foreach (Client_info ClientInfo in clientdata)
                {
                    string updateFollowupdateStr = string.Empty;

                    if (followupType == 1)
                    {
                        updateFollowupdateStr = "Update PlanYearEnd set Request_Follow_1='" + currentDate + "', Request_Follow_1_ByWho=" + CreatedbyWho + ", Request_Follow_1_Type=" + DeliveryType + " where Plans_Index_ID=" + ClientInfo.Plans_Index_ID + " and PYE_Year=" + ClientInfo.PYE_Year + " and IsPortalDeleted=" + 0;
                    }
                    if (followupType == 2)
                    {
                        updateFollowupdateStr = "Update PlanYearEnd set Request_Follow_2='" + currentDate + "', Request_Follow_2_ByWho=" + CreatedbyWho + ", Request_Follow_2_Type=" + DeliveryType + " where Plans_Index_ID=" + ClientInfo.Plans_Index_ID + " and PYE_Year=" + ClientInfo.PYE_Year + " and IsPortalDeleted=" + 0;
                    }
                    if (followupType == 3)
                    {
                        updateFollowupdateStr = "Update PlanYearEnd set Request_Follow_3='" + currentDate + "', Request_Follow_3_ByWho=" + CreatedbyWho + ", Request_Follow_3_Type=" + DeliveryType + " where Plans_Index_ID=" + ClientInfo.Plans_Index_ID + " and PYE_Year=" + ClientInfo.PYE_Year + " and IsPortalDeleted=" + 0;
                    }
                    if (followupType == 4)
                    {
                        updateFollowupdateStr = "Update PlanYearEnd set Request_Follow_4='" + currentDate + "', Request_Follow_4_ByWho=" + CreatedbyWho + ", Request_Follow_4_Type=" + DeliveryType + " where Plans_Index_ID=" + ClientInfo.Plans_Index_ID + " and PYE_Year=" + ClientInfo.PYE_Year + " and IsPortalDeleted=" + 0;
                    }
                    if (followupType == 5)
                    {
                        updateFollowupdateStr = "Update PlanYearEnd set Request_Init='" + currentDate + "', Request_Init_ByWho=" + CreatedbyWho + ", Request_Init_Type=" + DeliveryType + " where Plans_Index_ID=" + ClientInfo.Plans_Index_ID + " and PYE_Year=" + ClientInfo.PYE_Year + " and IsPortalDeleted=" + 0;
                    }
                    cmdStr = updateFollowupdateStr;
                    ExecuteSqlCommand(cmdStr, con);
                }
            }
        }

        public static List<LetterInfoModel> getContactAdvisorInfo(int Client_Id, int contact_Id, string contactType)
        {
            SqlConnection con = new SqlConnection(dbCon);
            string cmd = "";
            if(contactType=="Contact")
            {
                cmd = "Select c.Client_ID, c.Client_Name, c.Segmentation, c.Business_Type, p.Plan_Number,  \r\n" +
                           "ct.First_Name, ct.Contact_ID, ct.Last_Name, ct.MI, ct.Title, ct.[Credentials], \r\n" +
                           "(Case ct.Greeting When '' Then ct.First_Name Else ct.Greeting End) as Greeting_Text, ad.Address1, ad.Address2, ad.City,  \r\n" +
                           "s.State_Abrev, ad.Zip_Code,adphy.Address1 as PhysicalAddress1, adphy.Address2 as PhysicalAddress2, adphy.City as PhysicalCity,sphy.State_Abrev as PhysicalState_abv ,adphy.Zip_Code as PhysicalZipCode,cm.Business_Phone, cm.Phone, cm.Mobile_Phone, cm.Fax, cm.Email  \r\n" +
                           "from Client_Master c Left Join Plans p on c.Client_ID = p.Client_ID \r\n" +
                           "Left Join Contacts ct on c.Client_ID = ct.Client_ID  \r\n" +
                           "Left Join [Address] ad on ct.MailAddress = ad.Address_ID \r\n" +
                           "Left Join [Address] adphy on ct.Address = adphy.Address_ID \r\n" +
                           "Left Join States s on ad.State = s.States_ID \r\n" +
                           "Left Join States sphy on adphy.State = sphy.States_ID \r\n" +
                           "Left Join Contact_Method cm on ct.Contact_Method = cm.Contact_Method_ID  \r\n" +
                           "Where c.Client_ID = " + Client_Id + "  AND  ct.Contact_ID=" + contact_Id +"";
		
            }
            if(contactType=="Advisor")
            {
                cmd =  "Select c.Client_ID, c.Client_Name, c.Segmentation, c.Business_Type, p.Plan_Number,\r\n" + 
                       "ad.First_Name, ad.Advisor_ID, ad.Last_Name, ad.MI, ad.[Credentials], \r\n" + 
                       "adr.Address1, adr.Address2, adr.City, s.State_Abrev, adr.Zip_Code,adphy.Address1 as PhysicalAddress1, adphy.Address2 as PhysicalAddress2, adphy.City as PhysicalCity,sphy.State_Abrev as PhysicalState_abv ,adphy.Zip_Code as PhysicalZipCode,\r\n" + 
                       "cm.Business_Phone, cm.Phone, cm.Mobile_Phone, cm.Fax, cm.Email \r\n" + 
                       "from Client_Master c Left Join Plans p on c.Client_ID = p.Client_ID  \r\n" + 
                       "Left join Advisor_Client_Link acl on c.Client_ID=acl.Client_ID \r\n" + 
                       "Left join Advisor ad on acl.Advisor_ID=ad.Advisor_ID \r\n" + 
		               "Left Join [Address] adr on ad.MailAddress = adr.Address_ID \r\n" + 
		               "Left Join [Address] adphy on ad.Address = adphy.Address_ID \r\n" + 
                       "Left Join States s on adr.State = s.States_ID \r\n" + 
		               "Left Join States sphy on adphy.State = sphy.States_ID \r\n" + 
                       "Left Join Contact_Method cm on ad.Contact_Method = cm.Contact_Method_ID  \r\n" + 
                       "Where c.Client_ID = " + Client_Id + "  AND ad.Advisor_ID=" + contact_Id + " " ;
            }
            dt = ExecuteSqlCommand(cmd, con);
            List<LetterInfoModel> contactAdvisorData = ConvertDataTableToList<LetterInfoModel>(dt);
            return contactAdvisorData;
        }

        public static CommModule_AllPredefinedKey getContactAdvisorData(string htmlDoctemp, List<LetterInfoModel> contact_AdvisorData, CommModule_AllPredefinedKey predefinedKeyObj)
        {
            string dateOfRecord = DateTime.UtcNow.ToString("MMMM dd, yyyy");
            predefinedKeyObj.Date = dateOfRecord;
            predefinedKeyObj.FirstName = contact_AdvisorData[0].First_Name == null ? "" : contact_AdvisorData[0].First_Name;
            predefinedKeyObj.LastName = contact_AdvisorData[0].Last_Name == null ? "" : contact_AdvisorData[0].Last_Name;
            predefinedKeyObj.MI = contact_AdvisorData[0].MI == null ? "" : contact_AdvisorData[0].MI;
            predefinedKeyObj.Address1 = contact_AdvisorData[0].Address1 == null ? "" : contact_AdvisorData[0].Address1;
            predefinedKeyObj.Address2 = contact_AdvisorData[0].Address2 == null ? "" : contact_AdvisorData[0].Address2;
            predefinedKeyObj.City = contact_AdvisorData[0].City == null ? "" : contact_AdvisorData[0].City;
            predefinedKeyObj.State_abv = contact_AdvisorData[0].State_Abrev == null ? "" : contact_AdvisorData[0].State_Abrev;
            predefinedKeyObj.zipcode = contact_AdvisorData[0].Zip_Code == null ? "" : contact_AdvisorData[0].Zip_Code;
            predefinedKeyObj.PhysicalAddress1 = contact_AdvisorData[0].PhysicalAddress1 == null ? "" : contact_AdvisorData[0].PhysicalAddress1;
            predefinedKeyObj.PhysicalAddress2 = contact_AdvisorData[0].PhysicalAddress2 == null ? "" : contact_AdvisorData[0].PhysicalAddress2;
            predefinedKeyObj.PhysicalCity = contact_AdvisorData[0].City == null ? "" : contact_AdvisorData[0].City;
            predefinedKeyObj.PhysicalState_abv = contact_AdvisorData[0].PhysicalState_abv == null ? "" : contact_AdvisorData[0].PhysicalState_abv;
            predefinedKeyObj.PhysicalZipCode = contact_AdvisorData[0].PhysicalZipCode == null ? "" : contact_AdvisorData[0].PhysicalZipCode;
            predefinedKeyObj.Greetingtext = contact_AdvisorData[0].Greeting_Text == null ? "" : contact_AdvisorData[0].Greeting_Text;
            predefinedKeyObj.ClientId = contact_AdvisorData[0].Client_ID == null ? 0 : contact_AdvisorData[0].Client_ID;
            predefinedKeyObj.CilentName = contact_AdvisorData[0].Client_Name == null ? "" : contact_AdvisorData[0].Client_Name;
            predefinedKeyObj.UserName = predefinedKeyObj.FirstName + " " + predefinedKeyObj.LastName;
            return predefinedKeyObj;
        }
    }
}


public class CommModuleContactAdvisorDetails
{
    public int Id { get; set; }
    public string Name { get; set; }
    public int ClientId { get; set; }
    public string Email { get; set; }
    public bool ticked { get; set; }
    public bool msGroup { get; set; }
    public string Type { get; set; }
}

public class PortalDefaultsBlobDetails
{
    public int Id { get; set; }
    public string BlobContainerName { get; set; }
    public string BlobConnectionKey { get; set; }
    public string BlobConnectionString { get; set; }
}

public class PortalCommModuleScheduledEvents
{
    public int ScheduleId { get; set; }
    public int TemplateId { get; set; }
    public string TemplateBody { get; set; }
    public int NoticeType { get; set; }
    public int ScheduleType { get; set; }
    public int DeliveryType { get; set; }
    public string ClientId { get; set; }
    public string ContactIds { get; set; }
    public string ScheduleToList { get; set; }
    public string ScheduleCcList { get; set; }
    public string ScheduleWeekDays { get; set; }
    public Nullable<System.DateTime> ScheduleStartDate { get; set; }
    public Nullable<System.DateTime> ScheduleEndDate { get; set; }
    public Nullable<bool> IsActive { get; set; }
    public Nullable<bool> AddToCommLog { get; set; }
    public Nullable<bool> HoldFollowUps { get; set; }
    public Nullable<int> ScheduleCreatedBy { get; set; }
    public Nullable<System.DateTime> ScheduleCreatedDate { get; set; }
    public bool IsScheduled { get; set; }
    public Nullable<System.DateTime> DeliveredDate { get; set; }
    public Nullable<System.DateTime> NextDeliveryDate { get; set; }
    public string Attachments { get; set; }
    public Nullable<int> EndByOccurances { get; set; }
    public Nullable<int> OccurancesCompleted { get; set; }
    public Nullable<int> WeekOccurances { get; set; }
    public int AttachmentId { get; set; }
}

public class PortalCommModuleTemplates
{
    public int Id { get; set; }
    public string AzureTemplateId { get; set; }
    public string TemplateName { get; set; }
    public string TemplateSubject { get; set; }
    public string HtmlPath { get; set; }
    public int NoticeType { get; set; }
    public int CreatedBy { get; set; }
    public string CreatedDate { get; set; }
    public string TemplatePreHeader { get; set; }
    public int DeliveryType { get; set; }
    public string LastUpdatedDate { get; set; }
    public int LastUpdatedBy { get; set; }
    public bool IsDefault { get; set; }
    public int FollowupType { get; set; }

}

public class ClientIdEmailModel
{
    public int ClientId { get; set; }
    public string Email { get; set; }
    public string Type { get; set; }
    public int ContactAdvisorId { get; set; }
    public string ContactAdvisorName { get; set; }
    public string emailContent { get; set; }
}

public class CommLogPopUpModel
{
    public int CommLogId { get; set; }
    public int ClientId { get; set; }
    public string ClientName { get; set; }
    public int CommLogType { get; set; }
    public int Origin { get; set; }
    public string Contact1 { get; set; }
    public string Contact2 { get; set; }
    public string Contact3 { get; set; }
    public string PlanNumber { get; set; }
    public int Category { get; set; }
    public string SubCategory { get; set; }
    public decimal ActualTime { get; set; }
    public string CreateDate { get; set; }
    public bool IsDuplicate { get; set; }
    public bool IsInternalUseOnly { get; set; }
    public int AdHocToDo { get; set; }
    public string Subject { get; set; }
    public string Description { get; set; }
    public int AttachmentId { get; set; }
    public string LastUpdatedDateText { get; set; }
    public string LastUpdatedTimeText { get; set; }
    public int PlanIndexId { get; set; }
    public string NewDescription { get; set; }
    public int LastTouchedBy { get; set; }
    public string LastTouchedByName { get; set; }
}

public class Portal_CommLogContactAdvisor
{
    public int CommLogContactAdvisorId { get; set; }
    public int CommLogId { get; set; }
    public int ContactAdvisorId { get; set; }
    public string IsContactAdvisor { get; set; }
    public bool IsSelected { get; set; }
    public bool IsPortalDeleted { get; set; }

    public virtual Comm_Log Comm_Log { get; set; }
}

public class CommModule_Predefined
{
    public int Id { get; set; }
    public int TemplateId { get; set; }
    public string PredefinedKey { get; set; }
    public string PredefinedValue { get; set; }
}

public class LetterInfoModel
{
    public string Client_Name { get; set; }
    public string First_Name { get; set; }
    public string Last_Name { get; set; }
    public string Greeting_Text { get; set; }
    public string Address1 { get; set; }
    public string Address2 { get; set; }
    public string City { get; set; }
    public string State_Abrev { get; set; }
    public string Zip_Code { get; set; }
    public string Email { get; set; }

    public string PhysicalAddress1 { get; set; }
    public string PhysicalAddress2 { get; set; }
    public string PhysicalCity { get; set; }
    public string PhysicalState_abv { get; set; }
    public string PhysicalZipCode { get; set; }

    public string PlanYearEnd { get; set; }
    public string BusinessTypeText { get; set; }
    public string ServiceType { get; set; }
    public string PlanStatus { get; set; }
    public string Plan_Name { get; set; }
    public string PlanNumber { get; set; }
    public bool PrimaryContact { get; set; }
    public Int32 Plans_Index_Id { get; set; }
    public string PlanTypeText { get; set; }
    public string ConsultantFull { get; set; }
    public string ConsultantInitials { get; set; }
    public string ConsultantCertifications { get; set; }
    public string ConsultantEmail { get; set; }
    public string ConsultantPhone { get; set; }
    public string CSRFull { get; set; }
    public string CSRPhone { get; set; }
    public string CSREmail { get; set; }
    public int Plan_Type_Code { get; set; }
    public int CSR { get; set; }
    public string PlanYE { get; set; }
    public int Contact_Type { get; set; }
    public string MI { get; set; }
    public string CSRInitials { get; set; }

    public string CSRTitle { get; set; } // Added for New FollowUp letters.
    public int Client_ID { get; set; }
    public string SignaturePath { get; set; } // Added for New FollowUp letters.
    public int SignatureId { get; set; }

}
public class LetterCCInfoModel
{
    public string First_Name { get; set; }
    public string Last_Name { get; set; }
    public string MI { get; set; }
    public string Credentials { get; set; }
    public string Enclosures { get; set; }
    public string Via { get; set; }

    public Int32 Client_ID { get; set; }
    public string Email { get; set; }

    public int Contact_Method { get; set; }
    public int Contact_Type { get; set; }
    public int Contact_Type_Id { get; set; }
    public bool PrimaryContact { get; set; }
}
public class FollowUpLetterModel
{
    #region Public Properties

    /// <summary>
    /// Gets or sets the c account no text.
    /// </summary>
    public string CAccountNoText { get; set; }

    /// <summary>
    /// Gets or sets the custodian text.
    /// </summary>
    public string CustodianText { get; set; }

    /// <summary>
    /// Gets or sets the money type text.
    /// </summary>
    public string MoneyTypeText { get; set; }

    /// <summary>
    /// Gets or sets the month 01.
    /// </summary>
    public int Month01 { get; set; }

    /// <summary>
    /// Gets or sets the month 02.
    /// </summary>
    public int Month02 { get; set; }

    /// <summary>
    /// Gets or sets the month 03.
    /// </summary>
    public int Month03 { get; set; }

    /// <summary>
    /// Gets or sets the month 04.
    /// </summary>
    public int Month04 { get; set; }

    /// <summary>
    /// Gets or sets the month 05.
    /// </summary>
    public int Month05 { get; set; }

    /// <summary>
    /// Gets or sets the month 06.
    /// </summary>
    public int Month06 { get; set; }

    /// <summary>
    /// Gets or sets the month 07.
    /// </summary>
    public int Month07 { get; set; }

    /// <summary>
    /// Gets or sets the month 08.
    /// </summary>
    public int Month08 { get; set; }

    /// <summary>
    /// Gets or sets the month 09.
    /// </summary>
    public int Month09 { get; set; }

    /// <summary>
    /// Gets or sets the month 10.
    /// </summary>
    public int Month10 { get; set; }

    /// <summary>
    /// Gets or sets the month 11.
    /// </summary>
    public int Month11 { get; set; }

    /// <summary>
    /// Gets or sets the month 12.
    /// </summary>
    public int Month12 { get; set; }

    /// <summary>
    /// Gets or sets the more requested text.
    /// </summary>
    public string MoreRequestedText { get; set; }

    /// <summary>
    /// Gets or sets the rk account no text.
    /// </summary>
    public string RKAccountNoText { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether received 01.
    /// </summary>
    public bool Received01 { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether received 02.
    /// </summary>
    public bool Received02 { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether received 03.
    /// </summary>
    public bool Received03 { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether received 04.
    /// </summary>
    public bool Received04 { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether received 05.
    /// </summary>
    public bool Received05 { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether received 06.
    /// </summary>
    public bool Received06 { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether received 07.
    /// </summary>
    public bool Received07 { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether received 08.
    /// </summary>
    public bool Received08 { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether received 09.
    /// </summary>
    public bool Received09 { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether received 10.
    /// </summary>
    public bool Received10 { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether received 11.
    /// </summary>
    public bool Received11 { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether received 12.
    /// </summary>
    public bool Received12 { get; set; }

    /// <summary>
    /// Gets or sets the record keeper text.
    /// </summary>
    public string RecordKeeperText { get; set; }

    /// <summary>
    /// Gets or sets the statement delivery.
    /// </summary>
    public int StatementDelivery { get; set; }

    /// <summary>
    /// Gets or sets the tracking doc text.
    /// </summary>
    public string TrackingDocText { get; set; }

    /// <summary>
    /// Gets or sets the client id.
    /// </summary>
    public int Client_ID { get; set; }

    /// <summary>
    /// Gets or sets the SchA_Required.
    /// </summary>
    public bool SchARequired { get; set; }
    public bool NonQualifiedAsset { get; set; }
    public int InvestmentPlatform { get; set; }

    public int PYE_ID { get; set; } /* Feature #187 -Added By Amar to update Followup date on email Followup letter on 19-Feb-2016 */

    public int TrackingDoc_Type_Add { get; set; }//Added by nikita for individual sch a tracking

    public int PYE_Year { get; set; }//Added By Chandrashekhar
    public int Plans_Index_ID { get; set; }//Addeed By Chandrashekhar for market Valuation
    #endregion Public Properties
}

public class Contact_Info
{
    public int Contact_ID { get; set; }
    public int Client_ID { get; set; }
    public int Contact_No { get; set; }
    public int Contact_Type { get; set; }
    public int Contact_Type_ID { get; set; }
    public string First_Name { get; set; }
    public string Last_Name { get; set; }
    public string MI { get; set; }
    public string Greeting { get; set; }
    public string Credentials { get; set; }
    public int Contact_Method { get; set; }
    public int Contact_Method_ID { get; set; }
    public string Phone { get; set; }
    public string Phone_Ext { get; set; }
    public string Business_Phone { get; set; }
    public string Business_Phone_Ext { get; set; }
    public string Mobile_Phone { get; set; }
    public string Fax { get; set; }
    public string Email { get; set; }
    public int ClientMainAddress { get; set; }
    public int ClientMailAddress { get; set; }
    public int MainAddress { get; set; }
    public string MainAddress1 { get; set; }
    public string MainAddress2 { get; set; }
    public string MainCity { get; set; }
    public int? MainState { get; set; }
    public string MainState_Abrev { get; set; }
    public string MainState_Full { get; set; }
    public string MainZip { get; set; }
    public int MailAddress { get; set; }
    public string MailAddress1 { get; set; }
    public string MailAddress2 { get; set; }
    public string MailCity { get; set; }
    public int? MailState { get; set; }
    public string MailState_Abrev { get; set; }
    public string MailState_Full { get; set; }
    public string MailZip { get; set; }
    public int CC { get; set; }
    public string CCText { get; set; }
    public bool BoardMember { get; set; }
    public bool IsDonotContact { get; set; }
    public bool Trustee { get; set; }
    public bool StockHolder { get; set; }
    public bool PrimaryContact { get; set; }
    public bool CorporateOfficer { get; set; }
    public bool InvoiceContact { get; set; }
    public bool AdministrativeContact { get; set; }
    public decimal StockPercent { get; set; }
    public bool Sinor { get; set; }
    public string RegistrationEmail { get; set; }
    public string Title { get; set; }
    public string PrefixAbrev { get; set; }
    public string SuffixAbrev { get; set; }
    public int Prefix { get; set; }
    public int Suffix { get; set; }
    public string Enclosures { get; set; }
    //Same Client Address
    public bool IsSameClientMainAddress { get; set; }
    public bool IsSameClientMailAddress { get; set; }
    public bool IsSameAsAboveAddress { get; set; }

    public int ContactOrder { get; set; }
    public string Certifications { get; set; }
    public string ContactTypeStr { get; set; }
    //added for Change request
    public string ContactName { get; set; }

    public DateTime? LastTouchedClient { get; set; }
    public string LastTouchedClientString { get; set; }
    public string ByWhoString { get; set; }

    public string EffectiveDtCO { get; set; }
    public string EffectiveDtBM { get; set; }
    public string EffectiveDtTrustee { get; set; }
    public string EffectiveDtSH { get; set; }

    public string UDF1 { get; set; }
    public string UDF2 { get; set; }
    public string UDF3 { get; set; }
    public string UDF4 { get; set; }
    public string UDF5 { get; set; }
}

public class Client_info
{
    public string Client_Name { get; set; }
    public string Plan_Name { get; set; }
    public int PYE_Year { get; set; }
    public string PlanYE { get; set; }
    public int Plan_Number { get; set; }
    public int Plans_Index_ID { get; set; }
    public int Client_ID { get; set; }

}

public class PlanAssetModel
{
    public int PYE_ID { get; set; }
    public int AssetTracking_ID { get; set; }
    public int Plans_Index_ID { get; set; }
    public int PYE_Year { get; set; }
    public int Money_Type { get; set; }
    public int Asset_Index { get; set; }
    public int Account_Type { get; set; }
    public string Record_Keeper { get; set; }
    public string MoneyType { get; set; }
    public string Account_Type_Text { get; set; }
    public string RK_Account_No { get; set; }
    public string HasNotesGlyph { get; set; }
    public string Custodian { get; set; }
    public int Notes_ID { get; set; }
    public int Actual_Document { get; set; }
    public string statusColor { get; set; }
    public bool Sch_A_Required { get; set; }

    public string Kind { get; set; }
    public bool AccountClosed { get; set; }
    public DateTime LastStatementDate { get; set; }
    public string Statement_From_Text { get; set; }
    public string Last_Statement_Date_Text { get; set; }


    //Added By Amar To Reduce Annual Report Loading Time
    public int TrackingDoc_Type_Add { get; set; }
    public bool Verified { get; set; }
    public bool NotRequired { get; set; }
    public string Reviewed_Text { get; set; }
    public string More_Requested_Text { get; set; }
    public string Initially_Received_Text { get; set; }
    public string Request_Init_Text { get; set; }
    public string VerifiedWhen_Text { get; set; }
    public DateTime Initially_Received { get; set; }
    public DateTime More_Received { get; set; }
    public string More_Received_Text { get; set; }
    public string Request_Follow_1_Text { get; set; }
    public string Request_Follow_2_Text { get; set; }
    public string Request_Follow_3_Text { get; set; }
}
public class MarketValuationModel
{
    public int MarketValuation_ID { get; set; }
    public int PYE_ID { get; set; }
    public int AssetTracking_ID { get; set; }
    public int Plans_Index_ID { get; set; }
    public int PYE_Year { get; set; }
    public int Money_Type { get; set; }
    public int Asset_Index { get; set; }
    public int Account_Type { get; set; }
    public string Record_Keeper { get; set; }
    public string MoneyType { get; set; }
    public string Account_Type_Text { get; set; }
    public string RK_Account_No { get; set; }
    public string HasNotesGlyph { get; set; }
    public string Custodian { get; set; }
    public int Notes_ID { get; set; }
    public int Actual_Document { get; set; }
    public string statusColor { get; set; }
    public bool Sch_A_Required { get; set; }
    public bool NonQualifiedAsset { get; set; }
    public string Kind { get; set; }
    public bool AccountClosed { get; set; }
    public DateTime LastStatementDate { get; set; }
    public string Statement_From_Text { get; set; }
    public int Statement_Delivery { get; set; }
    public string Last_Statement_Date_Text { get; set; }
}
public class FollowupsValue
{
    public int Type5500 { get; set; }
    public int TaxedAs { get; set; }
    public string TaxedAs_Text { get; set; }
    public int TaxedAs_ID { get; set; }
    public int Plan_Type_Code { get; set; }
    public int Advisor_Type_ID { get; set; }
    public string InvestmentPlatform_Text { get; set; }

}
public class AdvisorEditModel
{
    public int Client_Id { get; set; }
    public int Plans_Index_Id { get; set; }
    public int Plan_Number { get; set; }
    public int Advisor_type_id { get; set; }
    public string Advisor_Type_Text { get; set; }
    public int Advisor_ID { get; set; }
    public string First_Name { get; set; }
    public string Last_Name { get; set; }
    public string Full_Name { get; set; }
    public string MI { get; set; }
    public string Credentials { get; set; }
    public int Contact_Method { get; set; }
    public string Phone { get; set; }
    public string Phone_Ext { get; set; }
    public string Business_Phone { get; set; }
    public string Business_Phone_Ext { get; set; }
    public string Mobile_Phone { get; set; }
    public string Fax { get; set; }
    public string Email { get; set; }
    public int? FirmMainAddress { get; set; }
    public int? FirmMailAddress { get; set; }
    public int MainAddress { get; set; }
    public string MainAddress1 { get; set; }
    public string MainAddress2 { get; set; }
    public string MainCity { get; set; }
    public int? MainState { get; set; }
    public string MainState_Abrev { get; set; }
    public string MainState_Full { get; set; }
    public string MainZip { get; set; }
    public int MailAddress { get; set; }
    public string MailAddress1 { get; set; }
    public string MailAddress2 { get; set; }
    public string MailCity { get; set; }
    public int? MailState { get; set; }
    public string MailState_Abrev { get; set; }
    public string MailState_Full { get; set; }
    public string MailZip { get; set; }
    public int CommunicationPreference { get; set; }
    //public int CCType { get; set; }
    public int Prefix { get; set; }
    public int Suffix { get; set; }
    public int AdvisorFirmID { get; set; }
    public string AdvisorFirmName { get; set; }

    //Same Client Address
    public bool IsSameFirmMainAddress { get; set; }
    public bool IsSameFirmMailAddress { get; set; }
    public bool IsSameAsAboveAddress { get; set; }
    public string PlanName { get; set; }
    public string Enclosures { get; set; }

    public DateTime? LastTouchedClient { get; set; }
    public string ByWhoString { get; set; }
    public string LastTouchedClientString { get; set; }

    public string UDF1 { get; set; }
    public string UDF2 { get; set; }
    public string UDF3 { get; set; }
    public string UDF4 { get; set; }
    public string UDF5 { get; set; }
}

public class CommModule_AllPredefinedKey
{
    public string UserName { get; set; }
    public int ClientId { get; set; }
    public int PlanId { get; set; }
    public string Followups { get; set; }
    public string Date { get; set; }
    public string FirstName { get; set; }
    public string MI { get; set; }
    public string LastName { get; set; }
    public string CilentName { get; set; }
    public string Address1 { get; set; }
    public string Address2 { get; set; }
    public string City { get; set; }
    public string State_abv { get; set; }
    public string zipcode { get; set; }

    public string PhysicalAddress1 { get; set; }
    public string PhysicalAddress2 { get; set; }
    public string PhysicalCity { get; set; }
    public string PhysicalState_abv { get; set; }
    public string PhysicalZipCode { get; set; }

    public string planName { get; set; }
    public string Greetingtext { get; set; }
    public string planyearEndDate { get; set; }
    public string StartDate { get; set; }
    public string EndDate { get; set; }
    public string secondndRushChgStartDate { get; set; }
    public string secondndRushChgEndDate { get; set; }

    public string PAame { get; set; }
    public string Title { get; set; }
    public string CsrInitial { get; set; }
    public string userinitial { get; set; }
    public string ccInformation { get; set; }
    public string extendeddate { get; set; }
    public string signature { get; set; }
    public string ContriButionDateFundingDeadline { get; set; }
    public string MinmumFundingDeadLine { get; set; }
    public string UnExtDate { get; set; }
    public string Header { get; set; }
    public string Footer { get; set; }
    public string FUP1 { get; set; }
    public string CSRFull { get; set; }
    public string CSRPhone { get; set; }
    public string CSREmail { get; set; }
    public string Ext5500_RequiredDate { get; set; }
    public string PlanNumber { get; set; }
    public string TPAUserImage { get; set; }
    public List<FollowUpLetterModel> followUpData { get; set; }


}
public class PortalCommModule_FollowupData
{
    public int Id { get; set; }
    public string DynamicKey { get; set; }
    public int Value { get; set; }
}

public class Comm_Log
{
    //public Comm_Log()
    //{
    //    this.Portal_CommLogContactAdvisor = new HashSet<Portal_CommLogContactAdvisor>();
    //}

    public int Comm_Log_ID { get; set; }
    public int Client_ID { get; set; }
    public int Comm_Log_Type { get; set; }
    public int Comm_Log_Origination { get; set; }
    public int Comm_Log_Category { get; set; }
    public string Sub_Category { get; set; }
    public int Comm_Log_Status { get; set; }
    public Nullable<int> Plan_ID { get; set; }
    public Nullable<int> Project_ID { get; set; }
    public Nullable<int> AdHocToDo { get; set; }
    public string Title { get; set; }
    public string CommContact1 { get; set; }
    public string CommContact2 { get; set; }
    public string CommContact3 { get; set; }
    public Nullable<int> Attachment_ID { get; set; }
    public string Comm_Log_Text { get; set; }
    public bool Duplicate { get; set; }
    public bool Internal_Use_Only { get; set; }
    public System.DateTime OriginDate { get; set; }
    public System.DateTime LastTouched { get; set; }
    public int ByWho { get; set; }
    public Nullable<decimal> Actual_Time { get; set; }
    public Nullable<int> CreatedByWho { get; set; }
    public bool IsDeletedFromOutlook { get; set; }
    public bool IsPortalDeleted { get; set; }
    public int ByWhoClient { get; set; }
    public System.DateTime LastTouchedClient { get; set; }
    public bool IsLastUpdatedByAdvisor { get; set; }

    //public virtual Comm_Log_Category Comm_Log_Category1 { get; set; }
    //public virtual UserTable UserTable { get; set; }
    //public virtual ICollection<Portal_CommLogContactAdvisor> Portal_CommLogContactAdvisor { get; set; }
}

public class GeneralIdNameModel
{
    public int ID { get; set; }
    public string Name { get; set; }
    public bool IsChecked { get; set; }
    public int Count { get; set; }
    public string UserInitial { get; set; }
    public int ContactMethod { get; set; }
    public string EmailId { get; set; }
    public int PlanTypeId { get; set; }
    public int ClientID { get; set; }
}






